﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Entidades;

namespace Business
{
    public class BusinessUsuario
    {
        public Usuario busca(Usuario usuario)
        {
            return new DaoUsuario().BuscaLogin(usuario.login, usuario.senha);
        }
        public void salvar(Usuario usuario)
        {
            new DaoUsuario().Salvar(usuario);
        }
        public void Alterar(Usuario usuario)
        {
            if (BusinessAlteraUsuario.Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do Usuário é obrigatório");
            }
            new DaoUsuario().Alterar(usuario);
        }

        public void Excluir(Usuario usuario)
        {
            DaoUsuario daoUsuario = new DaoUsuario();
            daoUsuario.Excluir(usuario);
        }
    }
}
