﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Entidades;

namespace Business
{
    public class BusinessOrdem
    {       
        public void salvar(Ordem ordem)
        {
            new DaoOrdem().Salvar(ordem);
        }

        public ClienteVO BuscaClienteID(int id)
        {
            return new DaoOrdem().BuscaClienteID(id);
        }
        public void Alterar(Ordem ordem)
        {
            if (Business.BusinessAlteraOrdem.defeitoReclamado.Trim().Length == 0)
            {
                throw new Exception("O nome do Ordem é obrigatório");
            }
            new DaoOrdem().Alterar(ordem);
        }
        public void Excluir(Ordem ordem)
        {
            DaoOrdem daoOrdem = new DaoOrdem();
            daoOrdem.Excluir(ordem);
        }
    }
}
