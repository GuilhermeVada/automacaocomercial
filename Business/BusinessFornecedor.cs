﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Dao;

namespace Business
{
    public class BusinessFornecedor
    {
        public void salvar(Fornecedor fornecedor)
        {
            new DaoFornecedor().Salvar(fornecedor);
        }
        public void Alterar(Fornecedor fornecedor)
        {
            if (BusinessAlteraFornecedor.razaoSocial.Trim().Length == 0)
            {
                throw new Exception("O nome do Cliente é obrigatória");
            }
            new DaoFornecedor().Alterar(fornecedor);
        }

        public void Excluir(Fornecedor fornecedor)
        {
            DaoFornecedor daoFornecedor = new DaoFornecedor();
            daoFornecedor.Excluir(fornecedor);
        }
    }
}
