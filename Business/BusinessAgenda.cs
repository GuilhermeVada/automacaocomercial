﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Entidades;

namespace Business
{
    public class BusinessAgenda
    {
        public void salvar(Agenda agenda)
        {
            new DaoAgenda().Salvar(agenda);
        }
        public void Alterar(Agenda agenda)
        {
            if (BusinessAlteraAgenda.Nome.Trim().Length == 0)
            {
                throw new Exception("O nome da Agenda é obrigatória");
            }
            new DaoAgenda().Alterar(agenda);
        }

        public void Excluir(Agenda agenda)
        {
            DaoAgenda daoAgenda = new DaoAgenda();
            daoAgenda.Excluir(agenda);
        }
    }
}
