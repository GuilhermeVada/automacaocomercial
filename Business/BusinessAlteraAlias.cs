﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class BusinessAlteraAlias
    {
        private static string codigo;
        private static string empresa = "";
        private static string servidor = "";
        private static string banco = "";
        public static string Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }
        public static string Empresa
        {
            get
            {
                return empresa;
            }
            set
            {
                empresa = value;
            }
        }

        public static string Servidor
        {
            get
            {
                return servidor;
            }
            set
            {
                servidor = value;
            }
        }

        public static string Banco
        {
            get
            {
                return banco;
            }
            set
            {
                banco = value;
            }
        }
    }
}
