﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class BusinessAlteraAgenda
    {
        private static string codigo;
        private static string nome = "";
        private static string email = "";
        private static string telefone = "";
        private static string celular = "";
        private static string fax = "";
        private static string anotacoes = "";


        public static string Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }
        public static string Nome
        {
            get
            {
                return nome;
            }
            set
            {
                nome = value;
            }
        }

        public static string Telefone
        {
            get
            {
                return telefone;
            }
            set
            {
                telefone = value;
            }
        }

        public static string Celular
        {
            get
            {
                return celular;
            }
            set
            {
                celular = value;
            }
        }

        public static string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }

        public static string Anotacoes
        {
            get
            {
                return anotacoes;
            }
            set
            {
                anotacoes = value;
            }
        }

        public static string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
    }
}