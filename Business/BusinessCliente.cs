﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Entidades;

namespace Business
{
    public class BusinessCliente
    {
        public void salvar(Cliente cliente)
        {
            new DaoCliente().Salvar(cliente);
        }

        public List<ClienteVO> Cliente(string param)
        {
            return new DaoCliente().Cliente(param);
        }

        public void Alterar(Cliente cliente)
        {
            if (BusinessAlteraCliente.Nome.Trim().Length == 0)
            {
                throw new Exception("O nome do Cliente é obrigatória");
            }
            new DaoCliente().Alterar(cliente);
        }

        public void Excluir(Cliente cliente)
        {
            DaoCliente daoCliente = new DaoCliente();
            daoCliente.Excluir(cliente);
        }
    }
}
