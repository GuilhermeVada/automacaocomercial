﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public static class BusinessAlteraOrdem
    {
        public static string idOrdem { get; set; }
        public static string idTecnico { get; set; }
        public static string idCliente { get; set; }
        public static string dtCadastro { get; set; }
        public static string prazoEntrega { get; set; }
        public static string situacao { get; set; }
        public static string tipoAtendimento { get; set; }
        public static string garantia { get; set; }
        public static string garantiaAte { get; set; }
        public static string local { get; set; }
        public static string defeitoReclamado { get; set; }
        public static string equipamentoDeixado { get; set; }
        public static string informacoesAdc { get; set; }
        public static string problemaConstatado { get; set; }
        public static string solucaoServico { get; set; }
        public static string valorProduto { get; set; }
        public static string valorServico { get; set; }
        public static string valorDeslocamento { get; set; }
        public static string totalOrdem { get; set; }
        public static string finalizacao { get; set; }
        public static string inicio { get; set; }
        public static string fim { get; set; }
        public static string contato { get; set; }
    }
}
