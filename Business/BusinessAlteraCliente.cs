﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public static class BusinessAlteraCliente
    {
        private static string codigo;
        private static string nome = "";
        private static string nascimento = "";
        private static string rg = "";
        private static string cpf = "";
        private static string naturalidade = "";
        private static string sexo = "";
        private static string email = "";
        private static string telefone = "";
        private static string celular = "";
        private static string fax = "";
        private static string estadoCivil = "";
        private static string contato = "";
        private static string endereco = "";
        private static string numero = "";
        private static string bairro = "";
        private static string pontoRefer = "";
        private static string complemento = "";
        private static string cidade = "";
        private static string uf = "";
        private static string cep = "";
        private static string ativo = "";
        private static string inscEstadual = "";
        private static string cnpj = "";
        private static string nacionalidade = "";

        public static string Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }
        public static string Nome
        {
            get
            {
                return nome;
            }
            set
            {
                nome = value;
            }
        }
        public static string Nascimento
        {
            get
            {
                return nascimento;
            }
            set
            {
                nascimento = value;
            }
        }

        public static string Rg
        {
            get
            {
                return rg;
            }
            set
            {
                rg = value;
            }
        }

        public static string Cpf
        {
            get
            {
                return cpf;
            }
            set
            {
                cpf = value;
            }
        }

        public static string Naturalidade
        {
            get
            {
                return naturalidade;
            }
            set
            {
                naturalidade = value;
            }
        }
        public static string Sexo
        {
            get
            {
                return sexo;
            }
            set
            {
                sexo = value;
            }
        }
        public static string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public static string Telefone
        {
            get
            {
                return telefone;
            }
            set
            {
                telefone = value;
            }
        }

        public static string Celular
        {
            get
            {
                return celular;
            }
            set
            {
                celular = value;
            }
        }

        public static string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }
        public static string EstadoCivil
        {
            get
            {
                return estadoCivil;
            }
            set
            {
                estadoCivil = value;
            }
        }

        public static string Contato
        {
            get
            {
                return contato;
            }
            set
            {
                contato = value;
            }
        }
        public static string Endereco
        {
            get
            {
                return endereco;
            }
            set
            {
                endereco = value;
            }
        }

        public static string Numero
        {
            get
            {
                return numero;
            }
            set
            {
                numero = value;
            }
        }
        public static string Bairro
        {
            get
            {
                return bairro;
            }
            set
            {
                bairro = value;
            }
        }

        public static string Complemento
        {
            get
            {
                return complemento;
            }
            set
            {
                complemento = value;
            }
        }

        public static string Cidade
        {
            get
            {
                return cidade;
            }
            set
            {
                cidade = value;
            }
        }

        public static string UF
        {
            get
            {
                return uf;
            }
            set
            {
                uf = value;
            }
        }

        public static string Cep
        {
            get
            {
                return cep;
            }
            set
            {
                cep = value;
            }
        }
        public static string PontoRef
        {
            get
            {
                return pontoRefer;
            }
            set
            {
                pontoRefer = value;
            }
        }
        public static string Ativo
        {
            get
            {
                return ativo;
            }
            set
            {
                ativo = value;
            }
        }
        public static string Cnpj
        {
            get
            {
                return cnpj;
            }
            set
            {
                cnpj = value;
            }
        }
        public static string InscEstadual
        {
            get
            {
                return inscEstadual;
            }
            set
            {
                inscEstadual = value;
            }
        }

        public static string Nacionalidade
        {
            get
            {
                return nacionalidade;
            }
            set
            {
                nacionalidade = value;
            }
        }





    }
}
