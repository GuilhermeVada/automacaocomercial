﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class BusinessAlteraFornecedor
    {
        public static string idFornecedor;
        public static string razaoSocial = "";
        public static string fantasia = "";
        public static string inscEstadual = "";
        public static string cnpj = "";
        public static string email = "";
        public static string telefone = "";
        public static string ramal = "";
        public static string fax = "";
        public static string contato = "";
        public static string endereco = "";
        public static string numero = "";
        public static string bairro = "";
        public static string pontoReferencia = "";
        public static string complemento = "";
        public static string cidade = "";
        public static string uf = "";
        public static string cep = "";
        public static string status = "";
        public static string IdFornecedor
        {
            get
            {
                return idFornecedor;
            }
            set
            {
                idFornecedor = value;
            }
        }
        public static string RazaoSocial    
        {
            get
            {
                return razaoSocial;
            }
            set
            {
                razaoSocial = value;
            }
        }
        public static string Fantasia
        {
            get
            {
                return fantasia;
            }
            set
            {
                fantasia = value;
            }
        }
        public static string InscricaoEstadual
        {
            get
            {
                return inscEstadual;
            }
            set
            {
                inscEstadual = value;
            }
        }
        public static string Cnpj
        {
            get
            {
                return cnpj;
            }
            set
            {
                cnpj = value;
            }
        }
        public static string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public static string Telefone
        {
            get
            {
                return telefone;
            }
            set
            {
                telefone = value;
            }
        }
        public static string Ramal
        {
            get
            {
                return ramal;
            }
            set
            {
                ramal = value;
            }
        }
        public static string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                fax = value;
            }
        }
        public static string Contato
        {
            get
            {
                return contato;
            }
            set
            {
                contato = value;
            }
        }
        public static string Endereco
        {
            get
            {
                return endereco;
            }
            set
            {
                endereco = value;
            }
        }
        public static string Numero
        {
            get
            {
                return numero;
            }
            set
            {
                numero = value;
            }
        }
        public static string Bairro
        {
            get
            {
                return bairro;
            }
            set
            {
                bairro = value;
            }
        }
        public static string PontoReferencia
        {
            get
            {
                return pontoReferencia;
            }
            set
            {
                pontoReferencia = value;
            }
        }
        public static string Complemento
        {
            get
            {
                return complemento;
            }
            set
            {
                complemento = value;
            }
        }
        public static string Cidade
        {
            get
            {
                return cidade;
            }
            set
            {
                cidade = value;
            }
        }
        public static string Uf
        {
            get
            {
                return uf;
            }
            set
            {
                uf = value;
            }
        }
        public static string Cep
        {
            get
            {
                return cep;
            }
            set
            {
                cep = value;
            }
        }
        public static string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }
    }
}
