﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dao;
using Entidades;

namespace Business
{
    public class BusinessAlias
    {
        public void salvar(Alias alias)
        {
            new DaoAlias().Salvar(alias);
        }
        public void Alterar(Alias alias)
        {
            if (BusinessAlteraAlias.Empresa.Trim().Length == 0)
            {
                throw new Exception("O nome da Agenda é obrigatória");
            }
            new DaoAlias().Alterar(alias);
        }

        public void Excluir(Alias alias)
        {
            DaoAlias daoAlias = new DaoAlias();
            daoAlias.Excluir(alias);
        }
    }
}
