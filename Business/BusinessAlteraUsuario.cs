﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class BusinessAlteraUsuario
    {
        private static string codigo;
        private static string nome = "";
        private static string telefone = "";
        private static string celular = "";
        private static string rg = "";
        private static string cpf = "";
        private static string endereco = "";
        private static string numero = "";
        private static string funcao = "";
        private static string nivelSistema = "";
        private static string email = "";
        private static string usuario = "";
        private static string senha = "";
        private static string statusSistema = "";
        public static string Codigo
        {
            get
            {
                return codigo;
            }
            set
            {
                codigo = value;
            }
        }
        public static string Nome
        {
            get
            {
                return nome;
            }
            set
            {
                nome = value;
            }
        }
        public static string Telefone
        {
            get
            {
                return telefone;
            }
            set
            {
                telefone = value;
            }
        }
        public static string Celular
        {
            get
            {
                return celular;
            }
            set
            {
                celular = value;
            }
        }
        public static string Rg
        {
            get
            {
                return rg;
            }
            set
            {
                rg = value;
            }
        }
        public static string Cpf
        {
            get
            {
                return cpf;
            }
            set
            {
                cpf = value;
            }
        }
        public static string Endereco
        {
            get
            {
                return endereco;
            }
            set
            {
                endereco = value;
            }
        }
        public static string Numero
        {
            get
            {
                return numero;
            }
            set
            {
                numero = value;
            }
        }
        public static string Funcao
        {
            get
            {
                return funcao;
            }
            set
            {
                funcao = value;
            }
        }
        public static string NivelSistema
        {
            get
            {
                return  nivelSistema;
            }
            set
            {
                nivelSistema = value;
            }
        }
        public static string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public static string Usuario
        {
            get
            {
                return usuario;
            }
            set
            {
                usuario = value;
            }
        }
        public static string Senha
        {
            get
            {
                return senha;
            }
            set
            {
                senha = value;
            }
        }
        public static string StatusSistema
        {
            get
            {
                return statusSistema;
            }
            set
            {
                statusSistema = value;
            }
        }
    }
}
