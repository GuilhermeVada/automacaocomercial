﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;
using System.Data.SqlClient;
using Business;


namespace Dao
{
    public class DaoUsuario: Conexao
    {
        public Usuario BuscaSenhaPermissao(string Senha)
        {

            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;


            command.CommandText = "SELECT SENHA, ID_USUARIO FROM USUARIO WHERE SENHA = @SENHA AND STATUS_SISTEMA = 'A'";
            command.Parameters.AddWithValue("@SENHA", Senha);
            cn.Open();
            SqlDataReader reader = command.ExecuteReader();
            Usuario usuario = null;
            while (reader.Read())
            {
                usuario = new Usuario();
                usuario.idUsuario = Convert.ToInt32(reader["ID_USUARIO"]);
            }
            cn.Close();
            return (usuario);
        }


        public Usuario BuscaLogin(string Login, string Senha)
        {
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "Select usuario,senha,id_Usuario from usuario where usuario = @USUARIO and senha = @SENHA AND STATUS_SISTEMA = 'A'";
            command.Parameters.AddWithValue("@USUARIO", Login);
            command.Parameters.AddWithValue("@SENHA", Senha);
            cn.Open();
            SqlDataReader reader = command.ExecuteReader();
            Usuario usuario = null;
            while (reader.Read())
            {
                usuario = new Usuario();
                usuario.idUsuario = Convert.ToInt32(reader["ID_USUARIO"]);
                usuario.login = Convert.ToString(reader["USUARIO"]);
            }
            cn.Close();
            return (usuario);
        }
        
        public List<Usuario> PesquisaUsuario()
        {
            SqlConnection cn = new SqlConnection();

            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "SELECT * FROM USUARIO";
            cn.Open();
            List<Usuario> usuarios = new List<Usuario>();
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Usuario usuario = new Usuario();
                usuario.idUsuario = Convert.ToInt32(reader["ID_USUARIO"]);
                usuario.login = Convert.ToString(reader["USUARIO"]);

                usuarios.Add(usuario);
            }
            return (usuarios);
        }
        
        public DataTable Listagem(string filtro)
        {
            DataTable tabela = new DataTable();
            string strSql;
            if (filtro == "")
            {
                strSql = "SELECT ID_USUARIO CÓDIGO, NOME, TELEFONE, CELULAR, EMAIL, NIVEL_SISTEMA = CASE NIVEL_SISTEMA WHEN '1' THEN 'Balconista' WHEN '2' THEN 'Caixa' WHEN '3' THEN 'Gerente' WHEN '4' THEN 'Proprietário' WHEN '5' THEN 'Sistema' END FROM USUARIO WHERE STATUS_SISTEMA = 'A' ORDER BY NOME";
            }
            else
            {
                strSql = "SELECT ID_USUARIO CÓDIGO, NOME, TELEFONE, CELULAR, EMAIL, NIVEL_SISTEMA = CASE NIVEL_SISTEMA WHEN '1' THEN 'Balconista' WHEN '2' THEN 'Caixa' WHEN '3' THEN 'Gerente' WHEN '4' THEN 'Proprietário' WHEN '5' THEN 'Sistema' END FROM USUARIO WHERE STATUS_SISTEMA = 'A' AND ID_USUARIO LIKE '%" + filtro + "%' or NOME LIKE '%" + filtro + "%' or TELEFONE LIKE '%" + filtro + "%' or CELULAR LIKE '%" + filtro + "%' ORDER BY NOME";
            }
            SqlDataAdapter da = new SqlDataAdapter(strSql, Conexao.StringDeConexao);
            da.Fill(tabela);
            return tabela;
        }

        public void Salvar(Usuario usuario)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            command.CommandText = "INSERT INTO USUARIO (NOME, TELEFONE, CELULAR, RG, CPF, ENDERECO, NUMERO, NIVEL_SISTEMA, FUNCAO, EMAIL, USUARIO, SENHA, STATUS_SISTEMA) VALUES (@NOME, @TELEFONE, @CELULAR, @RG, @CPF, @ENDERECO, @NUMERO, @NIVEL_SISTEMA, @FUNCAO, @EMAIL, @USUARIO, @SENHA, @STATUS_SISTEMA)";
            command.Parameters.AddWithValue("@NOME", usuario.nome.ToUpper());
            command.Parameters.AddWithValue("@TELEFONE", usuario.telefone.ToUpper());
            command.Parameters.AddWithValue("@CELULAR", usuario.celular.ToUpper());
            command.Parameters.AddWithValue("@RG", usuario.rg.ToUpper());
            command.Parameters.AddWithValue("@CPF", usuario.cpf.ToUpper());
            command.Parameters.AddWithValue("@ENDERECO", usuario.endereco.ToUpper());
            command.Parameters.AddWithValue("@NUMERO", usuario.numero.ToUpper());
            command.Parameters.AddWithValue("@FUNCAO", usuario.funcao.ToUpper());
            command.Parameters.AddWithValue("@NIVEL_SISTEMA", usuario.nivelSistema.ToUpper());
            command.Parameters.AddWithValue("@EMAIL", usuario.email.ToUpper());
            command.Parameters.AddWithValue("@USUARIO", usuario.login.ToUpper());
            command.Parameters.AddWithValue("@SENHA", usuario.senha.ToUpper());
            command.Parameters.AddWithValue("@STATUS_SISTEMA", usuario.statusSistema.ToUpper());
            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }
        public void Alterar(Usuario usuario)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "UPDATE USUARIO SET NOME = @NOME, TELEFONE = @TELEFONE, CELULAR = @CELULAR, RG = @RG, CPF = @CPF, ENDERECO = @ENDERECO, NUMERO = @NUMERO, FUNCAO = @FUNCAO, NIVEL_SISTEMA = @NIVEL_SISTEMA, EMAIL = @EMAIL, USUARIO = @USUARIO, SENHA = @SENHA, STATUS_SISTEMA = @STATUS_SISTEMA WHERE ID_USUARIO = " + Business.BusinessAlteraUsuario.Codigo;
                command.Parameters.AddWithValue("@NOME", usuario.nome.ToUpper());
                command.Parameters.AddWithValue("@TELEFONE", usuario.telefone.ToUpper());
                command.Parameters.AddWithValue("@CELULAR", usuario.celular.ToUpper());
                command.Parameters.AddWithValue("@RG", usuario.rg.ToUpper());
                command.Parameters.AddWithValue("@CPF", usuario.cpf.ToUpper());
                command.Parameters.AddWithValue("@ENDERECO", usuario.endereco.ToUpper());
                command.Parameters.AddWithValue("@NUMERO", usuario.numero.ToUpper());
                command.Parameters.AddWithValue("@FUNCAO", usuario.funcao.ToUpper());
                command.Parameters.AddWithValue("@NIVEL_SISTEMA", usuario.nivelSistema.ToUpper());
                command.Parameters.AddWithValue("@EMAIL", usuario.email.ToUpper());
                command.Parameters.AddWithValue("@USUARIO", usuario.login.ToUpper());
                command.Parameters.AddWithValue("@SENHA", usuario.senha.ToUpper());
                command.Parameters.AddWithValue("@STATUS_SISTEMA", usuario.statusSistema.ToUpper());
                cn.Open();
                command.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                throw new Exception("Servidor SQL Erro:" + ex.Number);
            }
        }
        public void Excluir(Usuario usuario)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "DELETE FROM USUARIO WHERE ID_USUARIO = " + BusinessAlteraUsuario.Codigo;
                command.Parameters.AddWithValue("@ID_USUARIO", BusinessAlteraUsuario.Codigo);
                cn.Open();
                int resultado = command.ExecuteNonQuery();
                if (resultado != 1)
                {
                    throw new Exception("Não foi possivel excluir o Usuário" + usuario.idUsuario);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL Erro:" + ex.Number);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {

                cn.Close();
            }
        }
    }    
}
