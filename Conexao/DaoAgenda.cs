﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entidades;
using Business;

namespace Dao
{
    public class DaoAgenda
    {
        public DataTable Listagem(string filtro)
        {
            DataTable tabela = new DataTable();
            string strSql;
            if (filtro == "")
            {
                strSql = "SELECT ID_AGENDA CÓDIGO, NOME, TELEFONE, CELULAR, FAX, EMAIL, ANOTACOES  FROM AGENDA ORDER BY NOME";
            }
            else
            {
                strSql = "SELECT ID_AGENDA CÓDIGO, NOME, TELEFONE, CELULAR, FAX, EMAIL, ANOTACOES FROM AGENDA WHERE ID_AGENDA LIKE '%" + filtro + "%' or NOME LIKE '%" + filtro + "%' or TELEFONE LIKE '%" + filtro + "%' or CELULAR LIKE '%" + filtro + "%' ORDER BY NOME ";
            }
            SqlDataAdapter da = new SqlDataAdapter(strSql, Conexao.StringDeConexao);
            da.Fill(tabela);
            return tabela;
        }

        public void Salvar(Agenda agenda)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "INSERT INTO AGENDA (NOME, TELEFONE, CELULAR, FAX, EMAIL, ANOTACOES) VALUES (@NOME, @TELEFONE, @CELULAR, @FAX, @EMAIL, @ANOTACOES)";
            command.Parameters.AddWithValue("@NOME", agenda.nome.ToUpper());
            command.Parameters.AddWithValue("@TELEFONE", agenda.telefone.ToUpper());
            command.Parameters.AddWithValue("@CELULAR", agenda.celular.ToUpper());
            command.Parameters.AddWithValue("@FAX", agenda.fax.ToUpper());
            command.Parameters.AddWithValue("@EMAIL", agenda.email.ToUpper());
            command.Parameters.AddWithValue("@ANOTACOES", agenda.anotacoes.ToUpper());

            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }
        public void Alterar(Agenda agenda)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "UPDATE AGENDA SET NOME = @NOME, TELEFONE = @TELEFONE, CELULAR = @CELULAR, FAX = @FAX, EMAIL = @EMAIL, ANOTACOES = @ANOTACOES WHERE ID_AGENDA = " + Business.BusinessAlteraAgenda.Codigo;
                command.Parameters.AddWithValue("@NOME", agenda.nome.ToUpper());
                command.Parameters.AddWithValue("@TELEFONE", agenda.telefone.ToUpper());
                command.Parameters.AddWithValue("@CELULAR", agenda.celular.ToUpper());
                command.Parameters.AddWithValue("@FAX", agenda.fax.ToUpper());
                command.Parameters.AddWithValue("@EMAIL", agenda.email.ToUpper());
                command.Parameters.AddWithValue("@ANOTACOES", agenda.anotacoes.ToUpper());
                cn.Open();
                command.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                throw new Exception("Servidor SQL Erro:" + ex.Number);
            }
        }
        public void Excluir(Agenda agenda)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "DELETE FROM AGENDA WHERE ID_AGENDA = " + BusinessAlteraAgenda.Codigo;
                command.Parameters.AddWithValue("@ID_AGENDA", BusinessAlteraAgenda.Codigo);
                cn.Open();
                int resultado = command.ExecuteNonQuery();
                if (resultado != 1)
                {
                    throw new Exception("Não foi possivel excluir a Agenda" + agenda.idCliente);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL Erro:" + ex.Number);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {

                cn.Close();
            }
        }
    }
}
