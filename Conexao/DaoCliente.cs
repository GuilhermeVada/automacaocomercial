﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entidades;
using Business;

namespace Dao
{
    public class DaoCliente : Conexao
    {
        SqlConnection con;
        public void Salvar(Cliente cliente)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "INSERT INTO CLIENTE (NOME, NATURALIDADE, RG, CPF, NASCIMENTO, SEXO, EMAIL, TELEFONE, CELULAR, FAX, ESTADO_CIVIL, CONTATO, ENDERECO, NUMERO, BAIRRO, PONTO_REF, COMPLEMENTO, CIDADE, UF, CEP, ATIVO, CNPJ, INSC_ESTADUAL, NACIONALIDADE) VALUES (@NOME, @NATURALIDADE, @RG, @CPF, @NASCIMENTO, @SEXO, @EMAIL, @TELEFONE, @CELULAR, @FAX, @ESTADO_CIVIL, @CONTATO, @ENDERECO, @NUMERO, @BAIRRO, @PONTO_REF, @COMPLEMENTO, @CIDADE, @UF, @CEP, @ATIVO, @CNPJ, @INSC_ESTADUAL, @NACIONALIDADE)";
            command.Parameters.AddWithValue("@NOME", cliente.nome.ToUpper());
            command.Parameters.AddWithValue("@NASCIMENTO", cliente.nascimento.ToUpper());
            command.Parameters.AddWithValue("@RG", cliente.rg.ToUpper());
            command.Parameters.AddWithValue("@CPF", cliente.cpf.ToUpper());
            command.Parameters.AddWithValue("@NATURALIDADE", cliente.naturalidade.ToUpper());
            command.Parameters.AddWithValue("@SEXO", cliente.sexo.ToUpper());
            command.Parameters.AddWithValue("@EMAIL", cliente.email.ToUpper());
            command.Parameters.AddWithValue("@TELEFONE", cliente.telefone.ToUpper());
            command.Parameters.AddWithValue("@CELULAR", cliente.celular.ToUpper());
            command.Parameters.AddWithValue("@FAX", cliente.fax.ToUpper());
            command.Parameters.AddWithValue("@ESTADO_CIVIL", cliente.estadoCivil.ToUpper());
            command.Parameters.AddWithValue("@CONTATO", cliente.contato.ToUpper());
            command.Parameters.AddWithValue("@ENDERECO", cliente.endereco.ToUpper());
            command.Parameters.AddWithValue("@NUMERO", cliente.numero.ToUpper());
            command.Parameters.AddWithValue("@BAIRRO", cliente.bairro.ToUpper());
            command.Parameters.AddWithValue("@PONTO_REF", cliente.pontoReferencia.ToUpper());
            command.Parameters.AddWithValue("@COMPLEMENTO", cliente.complemento.ToUpper());
            command.Parameters.AddWithValue("@CIDADE", cliente.cidade.ToUpper());
            command.Parameters.AddWithValue("@UF", cliente.uf.ToUpper());
            command.Parameters.AddWithValue("@CEP", cliente.cep.ToUpper());
            command.Parameters.AddWithValue("@ATIVO", cliente.ativo.ToUpper());
            command.Parameters.AddWithValue("@INSC_ESTADUAL", cliente.inscEstadual.ToUpper());
            command.Parameters.AddWithValue("@CNPJ", cliente.cnpj.ToUpper());
            command.Parameters.AddWithValue("@NACIONALIDADE", cliente.nacionalidade.ToUpper());

            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }
        
        public void Alterar(Cliente cliente)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "UPDATE CLIENTE SET NOME = @NOME, NATURALIDADE = @NATURALIDADE, RG = @RG, CPF = @CPF, NASCIMENTO = @NASCIMENTO, SEXO = @SEXO, EMAIL = @EMAIL, TELEFONE = @TELEFONE, CELULAR = @CELULAR, FAX = @FAX, ESTADO_CIVIL = @ESTADO_CIVIL, CONTATO = @CONTATO, ENDERECO = @ENDERECO, NUMERO = @NUMERO, BAIRRO = @BAIRRO, PONTO_REF = @PONTO_REF, COMPLEMENTO = @COMPLEMENTO, CIDADE = @CIDADE, UF = @UF, CEP = @CEP, ATIVO = @ATIVO, INSC_ESTADUAL = @INSC_ESTADUAL, CNPJ = @CNPJ, NACIONALIDADE = @NACIONALIDADE WHERE ID_CLIENTE = " + Business.BusinessAlteraCliente.Codigo;
                command.Parameters.AddWithValue("@NOME", cliente.nome.ToUpper());
                command.Parameters.AddWithValue("@NASCIMENTO", cliente.nascimento.ToUpper());
                command.Parameters.AddWithValue("@RG", cliente.rg.ToUpper());
                command.Parameters.AddWithValue("@CPF", cliente.cpf.ToUpper());
                command.Parameters.AddWithValue("@NATURALIDADE", cliente.naturalidade.ToUpper());
                command.Parameters.AddWithValue("@SEXO", cliente.sexo.ToUpper());
                command.Parameters.AddWithValue("@EMAIL", cliente.email.ToUpper());
                command.Parameters.AddWithValue("@TELEFONE", cliente.telefone.ToUpper());
                command.Parameters.AddWithValue("@CELULAR", cliente.celular.ToUpper());
                command.Parameters.AddWithValue("@FAX", cliente.fax.ToUpper());
                command.Parameters.AddWithValue("@ESTADO_CIVIL", cliente.estadoCivil.ToUpper());
                command.Parameters.AddWithValue("@CONTATO", cliente.contato.ToUpper());
                command.Parameters.AddWithValue("@ENDERECO", cliente.endereco.ToUpper());
                command.Parameters.AddWithValue("@NUMERO", cliente.numero.ToUpper());
                command.Parameters.AddWithValue("@BAIRRO", cliente.bairro.ToUpper());
                command.Parameters.AddWithValue("@PONTO_REF", cliente.pontoReferencia.ToUpper());
                command.Parameters.AddWithValue("@COMPLEMENTO", cliente.complemento.ToUpper());
                command.Parameters.AddWithValue("@CIDADE", cliente.cidade.ToUpper());
                command.Parameters.AddWithValue("@UF", cliente.uf.ToUpper());
                command.Parameters.AddWithValue("@CEP", cliente.cep.ToUpper());
                command.Parameters.AddWithValue("@ATIVO", cliente.ativo.ToUpper());
                command.Parameters.AddWithValue("@INSC_ESTADUAL", cliente.inscEstadual.ToUpper());
                command.Parameters.AddWithValue("@CNPJ", cliente.cnpj.ToUpper());
                command.Parameters.AddWithValue("@NACIONALIDADE", cliente.nacionalidade.ToUpper());
                cn.Open();
                command.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                throw new Exception("Servidor SQL Erro:" + ex.Number);
            }
        }

        public void Excluir(Cliente cliente)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "DELETE FROM CLIENTE WHERE ID_CLIENTE = " + BusinessAlteraCliente.Codigo;
                command.Parameters.AddWithValue("@ID_CLIENTE", BusinessAlteraCliente.Codigo);
                cn.Open();
                int resultado = command.ExecuteNonQuery();
                if (resultado != 1)
                {
                    throw new Exception("Não foi possivel excluir o Cliente" + cliente.idCliente);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL Erro:" + ex.Number);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {

                cn.Close();
            }
        }

        public List<ClienteVO> Cliente(string param)
        {
            using(con = new SqlConnection(StringDeConexao))
            {
                List<ClienteVO> lista = new List<ClienteVO>();
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT ID_CLIENTE CODIGO, NOME, ENDERECO, NUMERO, BAIRRO, CIDADE, CONTATO, TELEFONE, CELULAR ");
                sb.Append("FROM CLIENTE ");
                int numero;
                if(Int32.TryParse(param, out numero))
                {
                    sb.Append(string.Format("WHERE ID_CLIENTE = {0} ", numero));
                }
                else
                {
                    sb.Append(string.Format("WHERE NOME LIKE '%{0}%' ", param));
                
                    //sb.Append(string.Format("AND TELEFONE LIKE '%{0}%'", param));
                
                  //  sb.Append(string.Format("AND CELULAR LIKE '%{0}%' ", param));
                  //  sb.Append(string.Format("AND CONTATO LIKE '%{0}%' ", param));
                }
                con.Open();
                SqlCommand cmd = new SqlCommand(sb.ToString(), con);
                SqlDataReader leitor = cmd.ExecuteReader();
                if(leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        ClienteVO cliente = new ClienteVO();
                        cliente.idCliente = Convert.ToInt32(leitor["CODIGO"].ToString());
                        cliente.Nome = leitor["NOME"].ToString();
                        cliente.Endereco = leitor["ENDERECO"].ToString();
                        cliente.Numero = leitor["NUMERO"].ToString();
                        cliente.Bairro = leitor["BAIRRO"].ToString();
                        cliente.Cidade = leitor["CIDADE"].ToString();
                        cliente.Contato = leitor["CONTATO"].ToString();
                        cliente.Telefone = leitor["TELEFONE"].ToString();
                        cliente.Celular = leitor["CELULAR"].ToString();
                        lista.Add(cliente);
                    }
                    con.Close();
                }
                return lista;
            }
        }        
    }
}