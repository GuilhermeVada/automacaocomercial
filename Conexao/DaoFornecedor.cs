﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entidades;
using Business;

namespace Dao
{
    public class DaoFornecedor : Conexao
    {
        public DataTable Listagem(string filtro)
        {
            DataTable tabela = new DataTable();
            string strSql;
            if (filtro == "")
            {
                strSql = "SELECT ID_FORNECEDOR CÓDIGO, FANTASIA, RAZAO_SOCIAL RAZAOSOCIAL, EMAIL, TELEFONE, RAMAL, FAX, CONTATO FROM FORNECEDOR ORDER BY FANTASIA";
            }
            else
            {
                strSql = "SELECT ID_FORNECEDOR CÓDIGO, FANTASIA, RAZAO_SOCIAL RAZAOSOCIAL, EMAIL, TELEFONE, RAMAL, FAX, CONTATO FROM FORNECEDOR WHERE ID_FORNECEDOR LIKE '%" + filtro + "%' or FANTASIA LIKE '%" + filtro + "%'  or RAZAO_SOCIAL LIKE '%" + filtro + "%' or TELEFONE LIKE '%" + filtro + "%' or CONTATO LIKE '%" + filtro + "%' ORDER BY FANTASIA ";
            }
            SqlDataAdapter da = new SqlDataAdapter(strSql, Conexao.StringDeConexao);
            da.Fill(tabela);
            return tabela;
        }

        public void Salvar(Fornecedor fornecedor)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "INSERT INTO FORNECEDOR (RAZAO_SOCIAL, FANTASIA, INSC_ESTADUAL, CNPJ, EMAIL, TELEFONE, RAMAL, FAX, CONTATO, ENDERECO, NUMERO, BAIRRO, PONTO_REF, COMPLEMENTO, CIDADE, UF, CEP, STATUS_SISTEMA) VALUES (@RAZAO_SOCIAL, @FANTASIA, @INSC_ESTADUAL, @CNPJ, @EMAIL, @TELEFONE, @RAMAL, @FAX, @CONTATO, @ENDERECO, @NUMERO, @BAIRRO, @PONTO_REF, @COMPLEMENTO, @CIDADE, @UF, @CEP, @STATUS_SISTEMA)";
            command.Parameters.AddWithValue("@RAZAO_SOCIAL", fornecedor.razaoSocial.ToUpper());
            command.Parameters.AddWithValue("@FANTASIA", fornecedor.fantasia.ToUpper());
            command.Parameters.AddWithValue("@INSC_ESTADUAL", fornecedor.inscEstadual.ToUpper());
            command.Parameters.AddWithValue("@CNPJ", fornecedor.cnpj.ToUpper());
            command.Parameters.AddWithValue("@EMAIL", fornecedor.email.ToUpper());
            command.Parameters.AddWithValue("@TELEFONE", fornecedor.telefone.ToUpper());
            command.Parameters.AddWithValue("@RAMAL", fornecedor.ramal.ToUpper());
            command.Parameters.AddWithValue("@FAX", fornecedor.fax.ToUpper());
            command.Parameters.AddWithValue("@CONTATO", fornecedor.contato.ToUpper());
            command.Parameters.AddWithValue("@ENDERECO", fornecedor.endereco.ToUpper());
            command.Parameters.AddWithValue("@NUMERO", fornecedor.numero.ToUpper());
            command.Parameters.AddWithValue("@BAIRRO", fornecedor.bairro.ToUpper());
            command.Parameters.AddWithValue("@PONTO_REF", fornecedor.pontoReferencia.ToUpper());
            command.Parameters.AddWithValue("@COMPLEMENTO", fornecedor.complemento.ToUpper());
            command.Parameters.AddWithValue("@CIDADE", fornecedor.cidade.ToUpper());
            command.Parameters.AddWithValue("@UF", fornecedor.uf.ToUpper());
            command.Parameters.AddWithValue("@CEP", fornecedor.cep.ToUpper());
            command.Parameters.AddWithValue("@STATUS_SISTEMA", fornecedor.status.ToUpper());

            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }
        public void Alterar(Fornecedor fornecedor)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "UPDATE FORNECEDOR SET RAZAO_SOCIAL = @RAZAO_SOCIAL, FANTASIA = @FANTASIA, INSC_ESTADUAL = @INSC_ESTADUAL, CNPJ = @CNPJ, EMAIL = @EMAIL, TELEFONE = @TELEFONE, RAMAL = @RAMAL, FAX = @FAX, CONTATO = @CONTATO, ENDERECO = @ENDERECO, NUMERO = @NUMERO, BAIRRO = @BAIRRO, PONTO_REF = @PONTO_REF, COMPLEMENTO = @COMPLEMENTO, CIDADE = @CIDADE, UF = @UF, CEP = @CEP, STATUS_SISTEMA = @STATUS_SISTEMA WHERE ID_FORNECEDOR = " + Business.BusinessAlteraFornecedor.idFornecedor;
                command.Parameters.AddWithValue("@RAZAO_SOCIAL", fornecedor.razaoSocial.ToUpper());
                command.Parameters.AddWithValue("@FANTASIA", fornecedor.fantasia.ToUpper());
                command.Parameters.AddWithValue("@INSC_ESTADUAL", fornecedor.inscEstadual.ToUpper());
                command.Parameters.AddWithValue("@CNPJ", fornecedor.cnpj.ToUpper());
                command.Parameters.AddWithValue("@EMAIL", fornecedor.email.ToUpper());
                command.Parameters.AddWithValue("@TELEFONE", fornecedor.telefone.ToUpper());
                command.Parameters.AddWithValue("@RAMAL", fornecedor.ramal.ToUpper());
                command.Parameters.AddWithValue("@FAX", fornecedor.fax.ToUpper());
                command.Parameters.AddWithValue("@CONTATO", fornecedor.contato.ToUpper());
                command.Parameters.AddWithValue("@ENDERECO", fornecedor.endereco.ToUpper());
                command.Parameters.AddWithValue("@NUMERO", fornecedor.numero.ToUpper());
                command.Parameters.AddWithValue("@BAIRRO", fornecedor.bairro.ToUpper());
                command.Parameters.AddWithValue("@PONTO_REF", fornecedor.pontoReferencia.ToUpper());
                command.Parameters.AddWithValue("@COMPLEMENTO", fornecedor.complemento.ToUpper());
                command.Parameters.AddWithValue("@CIDADE", fornecedor.cidade.ToUpper());
                command.Parameters.AddWithValue("@UF", fornecedor.uf.ToUpper());
                command.Parameters.AddWithValue("@CEP", fornecedor.cep.ToUpper());
                command.Parameters.AddWithValue("@STATUS_SISTEMA", fornecedor.status.ToUpper());
                cn.Open();
                command.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                throw new Exception("Servidor SQL Erro:" + ex.Number);
            }
        }
        public void Excluir(Fornecedor fornecedor)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "DELETE FROM FORNECEDOR WHERE ID_FORNECEDOR = " + BusinessAlteraFornecedor.idFornecedor;
                command.Parameters.AddWithValue("@ID_FORNECEDOR", BusinessAlteraFornecedor.idFornecedor);
                cn.Open();
                int resultado = command.ExecuteNonQuery();
                if (resultado != 1)
                {
                    throw new Exception("Não foi possivel excluir o Fornecedor" + fornecedor.idFornecedor);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL Erro:" + ex.Number);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {
                cn.Close();
            }
        }
    }
}
