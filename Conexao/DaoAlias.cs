﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Business;
using Entidades;

namespace Dao
{
    public class DaoAlias
    {
        public DataTable Listagem(string filtro)
        {
            DataTable tabela = new DataTable();
            string strSql;
            if (filtro == "")
            {
                strSql = "SELECT ID_ALIAS CÓDIGO, EMPRESA, BANCO, SERVIDOR FROM ALIAS ORDER BY EMPRESA";
            }
            else
            {
                strSql = "SELECT ID_ALIAS CÓDIGO, EMPRESA, BANCO, SERVIDOR FROM ALIAS ORDER BY EMPRESA";
            }
            SqlDataAdapter da = new SqlDataAdapter(strSql, Conexao.StringDeConexao);
            da.Fill(tabela);
            return tabela;
        }

        public void Salvar(Alias alias)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "INSERT INTO ALIAS (EMPRESA, BANCO, SERVIDOR) VALUES (@EMPRESA, @BANCO, @SERVIDOR)";
            command.Parameters.AddWithValue("@EMPRESA", alias.empresa.ToUpper());
            command.Parameters.AddWithValue("@BANCO", alias.banco.ToUpper());
            command.Parameters.AddWithValue("@SERVIDOR", alias.servidor.ToUpper());

            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }

        public void Alterar(Alias alias)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "UPDATE ALIAS SET EMPRESA = @EMPRESA, BANCO = @BANCO, SERVIDOR= @SERVIDOR WHERE ID_ALIAS = " + Business.BusinessAlteraAlias.Codigo;
                command.Parameters.AddWithValue("@EMPRESA", alias.empresa.ToUpper());
                command.Parameters.AddWithValue("@BANCO", alias.banco.ToUpper());
                command.Parameters.AddWithValue("@SERVIDOR", alias.servidor.ToUpper());
                cn.Open();
                command.ExecuteNonQuery();
                cn.Close();
            }
            catch (SqlException ex)
            {
                throw new Exception("Servidor SQL Erro:" + ex.Number);
            }
        }
        public void Excluir(Alias alias)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "DELETE FROM ALIAS WHERE ID_ALIAS = " + BusinessAlteraAlias.Codigo;
                command.Parameters.AddWithValue("@ID_ALIAS", BusinessAlteraAlias.Codigo);
                cn.Open();
                int resultado = command.ExecuteNonQuery();
                if (resultado != 1)
                {
                    throw new Exception("Não foi possivel excluir a Agenda" + alias.idAlias);
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL Erro:" + ex.Number);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {

                cn.Close();
            }
        }
    }
}
