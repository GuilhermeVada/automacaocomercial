﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Configuration.Assemblies;
using Microsoft.Win32;

namespace Dao
{
    public class Conexao
    {

        public static string StringDeConexao
        {
            get
            {
                RegistryKey registro = Registry.CurrentUser.OpenSubKey("SISTEMA", true);
                // Retorna dados de um registro
                RegistryKey pRegKey = Registry.LocalMachine;
                pRegKey = registro;
                object banco = pRegKey.GetValue("BANCO");
                object caminho_servidor = pRegKey.GetValue("CAMINHO_SERVIDOR");
                return "Data Source=" + caminho_servidor + "; Initial Catalog=" + banco + ";User Id=sa;Password=@p@ssw0rd#";
            }
        }
    }
}
