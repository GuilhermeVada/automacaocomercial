﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Business;
using System.Data;
using System.Data.SqlClient;

namespace Dao
{
    public class DaoOrdem : Conexao
    {
        public DataTable Listagem(string filtro)
        {
            DataTable tabela = new DataTable();
            string strSql;
            if (filtro == "")
            {
                strSql = "SELECT O.ID_ORDEM CODIGO, E.NOME, O.DT_CADASTRO [DATA DE CADASTRO], O.PRAZO_ENTREGA [PRAZO DE ENTREGA], O.DEFEITO_RECLAMADO DEFEITO, O.CONTATO, E.TELEFONE FROM	ORDEM_SERVICO O, CLIENTE E WHERE	ISNULL(O.FINALIZACAO,'') = '' AND E.ID_CLIENTE = O.ID_CODCLIENTE ORDER BY O.ID_ORDEM";
            }
            else
            {
                strSql = "SELECT O.ID_ORDEM CODIGO, E.NOME, O.DT_CADASTRO [DATA DE CADASTRO], O.PRAZO_ENTREGA [PRAZO DE ENTREGA], O.DEFEITO_RECLAMADO DEFEITO, O.CONTATO, E.TELEFONE FROM	ORDEM_SERVICO O, CLIENTE E WHERE	ISNULL(O.FINALIZACAO,'') = '' AND E.ID_CLIENTE = O.ID_CODCLIENTE ORDER BY O.ID_ORDEM";
            }
            SqlDataAdapter da = new SqlDataAdapter(strSql, Conexao.StringDeConexao);
            da.Fill(tabela);
            return tabela;
        }
        public ClienteVO BuscaClienteID(int id)
        {
            using (con = new SqlConnection(Conexao.StringDeConexao))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT ID_CLIENTE, NOME, ENDERECO, NUMERO, BAIRRO, CIDADE, CONTATO, TELEFONE, CELULAR ");
                sb.Append("FROM CLIENTE ");
                sb.Append("WHERE ID_CLIENTE = @id");
                SqlCommand cmd = new SqlCommand(sb.ToString(), con);
                cmd.Parameters.AddWithValue("@id", id);
                con.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                ClienteVO c = null;
                if (leitor.HasRows)
                {
                    while (leitor.Read())
                    {
                        c = new ClienteVO();
                        c.idCliente = Convert.ToInt32(leitor["ID_CLIENTE"].ToString());
                        c.Nome = leitor["NOME"].ToString();
                        c.Endereco = leitor["ENDERECO"].ToString();
                        c.Numero = leitor["NUMERO"].ToString();
                        c.Bairro = leitor["BAIRRO"].ToString();
                        c.Cidade = leitor["CIDADE"].ToString();
                        c.Contato = leitor["CONTATO"].ToString();
                        c.Telefone = leitor["TELEFONE"].ToString();
                        c.Celular = leitor["CELULAR"].ToString();
                    }
                }
                con.Close();
                return c;
            }
        }
        SqlConnection con;
        public void Salvar(Ordem ordem)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "INSERT INTO ORDEM_SERVICO (ID_CODTECNICO, ID_CODCLIENTE, DT_CADASTRO, PRAZO_ENTREGA, GARANTIA, LOCAL, DEFEITO_RECLAMADO, EQUIPAMENTO_DEIXADO, INFORMACOES_ADC, PROBLEMA_CONTATADO, SOLUCAO_SERVICO, VALOR_PRODUTO, VALOR_SERVICO, VALOR_DESLOCAMENTO,CONTATO, FINALIZACAO, INICIO, FIM) VALUES (@ID_CODTECNICO, @ID_CODCLIENTE, @DT_CADASTRO, @PRAZO_ENTREGA, @GARANTIA, @LOCAL, @DEFEITO_RECLAMADO, @EQUIPAMENTO_DEIXADO, @INFORMACOES_ADC, @PROBLEMA_CONTATADO, @SOLUCAO_SERVICO, @VALOR_PRODUTO, @VALOR_SERVICO, @VALOR_DESLOCAMENTO, @CONTATO, @FINALIZACAO, @INICIO, @FIM)";
            command.Parameters.AddWithValue("@ID_CODTECNICO", ordem.idTecnico.ToString());
            command.Parameters.AddWithValue("@ID_CODCLIENTE", ordem.idCliente.ToString());
            command.Parameters.AddWithValue("@DT_CADASTRO", ordem.dtCadastro.ToUpper());
            command.Parameters.AddWithValue("@PRAZO_ENTREGA", ordem.prazoEntrega.ToUpper());
            command.Parameters.AddWithValue("@GARANTIA", ordem.garantia.ToUpper());
            command.Parameters.AddWithValue("@LOCAL", ordem.local.ToUpper());
            command.Parameters.AddWithValue("@CONTATO", ordem.contato.ToUpper());
            command.Parameters.AddWithValue("@DEFEITO_RECLAMADO", ordem.defeitoReclamado.ToUpper());
            command.Parameters.AddWithValue("@EQUIPAMENTO_DEIXADO", ordem.equipamentoDeixado.ToUpper());
            command.Parameters.AddWithValue("@INFORMACOES_ADC", ordem.informacoesAdc.ToUpper());
            command.Parameters.AddWithValue("@PROBLEMA_CONTATADO", ordem.problemaConstatado.ToUpper());
            command.Parameters.AddWithValue("@SOLUCAO_SERVICO", ordem.solucaoServico.ToUpper());
            command.Parameters.AddWithValue("@VALOR_PRODUTO", ordem.valorProduto.ToUpper());
            command.Parameters.AddWithValue("@VALOR_SERVICO", ordem.valorServico.ToUpper());
            command.Parameters.AddWithValue("@VALOR_DESLOCAMENTO", ordem.valorDeslocamento.ToUpper());
            command.Parameters.AddWithValue("@TOTAL_ORDEM", ordem.totalOrdem.ToUpper());
            command.Parameters.AddWithValue("@FINALIZACAO", ordem.finalizacao.ToUpper());
            command.Parameters.AddWithValue("@INICIO", ordem.inicio.ToUpper());
            command.Parameters.AddWithValue("@FIM", ordem.fim.ToUpper());

            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }

        public void Alterar(Ordem ordem)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;

            command.CommandText = "UPDATE ORDEM_SERVICO SET ID_CODTECNICO = @ID_CODTECNICO, ID_CODCLIENTE = @ID_CODCLIENTE, DT_CADASTRO = @DT_CADASTRO, PRAZO_ENTREGA = @PRAZO_ENTREGA, GARANTIA = @GARANTIA, LOCAL = @LOCAL, CONTATO = @CONTATO, DEFEITO_RECLAMADO = @DEFEITO_RECLAMADO, EQUIPAMENTO_DEIXADO = @EQUIPAMENTO_DEIXADO, INFORMACOES_ADC = @INFORMACOES_ADC, PROBLEMA_CONTATADO = @PROBLEMA_CONTATADO, SOLUCAO_SERVICO = @SOLUCAO_SERVICO, VALOR_PRODUTO = @VALOR_PRODUTO, VALOR_SERVICO = @VALOR_SERVICO, VALOR_PRODUTO = @VALOR_PRODUTO, VALOR_SERVICO = @VALOR_SERVICO, VALOR_DESLOCAMENTO = @VALOR_DESLOCAMENTO, = @TOTAL_ORDEM, FINALIZACAO = FINALIZACAO, INICIO = @ININIO, FIM = @FIM  WHERE ID_ORDEM =" + Business.BusinessAlteraOrdem.idOrdem;
            command.Parameters.AddWithValue("@ID_CODTECNICO", ordem.idTecnico.ToString());
            command.Parameters.AddWithValue("@ID_CODCLIENTE", ordem.idCliente.ToString());
            command.Parameters.AddWithValue("@DT_CADASTRO", ordem.dtCadastro.ToUpper());
            command.Parameters.AddWithValue("@PRAZO_ENTREGA", ordem.prazoEntrega.ToUpper());
            command.Parameters.AddWithValue("@GARANTIA", ordem.garantia.ToUpper());
            command.Parameters.AddWithValue("@LOCAL", ordem.local.ToUpper());
            command.Parameters.AddWithValue("@CONTATO", ordem.contato.ToUpper());
            command.Parameters.AddWithValue("@DEFEITO_RECLAMADO", ordem.defeitoReclamado.ToUpper());
            command.Parameters.AddWithValue("@EQUIPAMENTO_DEIXADO", ordem.equipamentoDeixado.ToUpper());
            command.Parameters.AddWithValue("@INFORMACOES_ADC", ordem.informacoesAdc.ToUpper());
            command.Parameters.AddWithValue("@PROBLEMA_CONTATADO", ordem.problemaConstatado.ToUpper());
            command.Parameters.AddWithValue("@SOLUCAO_SERVICO", ordem.solucaoServico.ToUpper());
            command.Parameters.AddWithValue("@VALOR_PRODUTO", ordem.valorProduto.ToUpper());
            command.Parameters.AddWithValue("@VALOR_SERVICO", ordem.valorServico.ToUpper());
            command.Parameters.AddWithValue("@VALOR_DESLOCAMENTO", ordem.valorDeslocamento.ToUpper());
            command.Parameters.AddWithValue("@TOTAL_ORDEM", ordem.totalOrdem.ToUpper());
            command.Parameters.AddWithValue("@FINALIZACAO", ordem.finalizacao.ToUpper());
            command.Parameters.AddWithValue("@INICIO", ordem.inicio.ToUpper());
            command.Parameters.AddWithValue("@FIM", ordem.fim.ToUpper());

            cn.Open();
            command.ExecuteNonQuery();
            cn.Close();
        }
        public void Excluir(Ordem ordem)
        {
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = Conexao.StringDeConexao;
            SqlCommand command = new SqlCommand();
            command.Connection = cn;
            try
            {
                command.CommandText = "DELETE ORDEM_SERVICO WHERE ID_ORDEM = " + BusinessAlteraOrdem.idOrdem;
                command.Parameters.AddWithValue("@ID_ORDEM", BusinessAlteraOrdem.idOrdem);
                cn.Open();
                int resultado = command.ExecuteNonQuery();
                if (resultado != 1)
                {
                    throw new Exception("Não foi possivel excluir a O.S." + ordem.idOrdem );
                }
            }
            catch (SqlException ex)
            {
                throw new Exception("SQL Erro:" + ex.Number);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
            finally
            {

                cn.Close();
            }
        }
    }
}
