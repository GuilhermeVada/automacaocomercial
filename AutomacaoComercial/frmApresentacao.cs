﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using Microsoft.Win32;
using DFW;

namespace AutomacaoComercial
{
    public partial class frmApresentacao : Form
    {
        public frmApresentacao()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Close();
        }

        private void frmApresentacao_Load(object sender, EventArgs e)
        {
            RegistryKey registro = Registry.CurrentUser.OpenSubKey("SISTEMA", true);
            // Retorna dados de um registro
            RegistryKey pRegKey = Registry.LocalMachine;
            pRegKey = registro;
            object pasta = pRegKey.GetValue("CAMINHO_PASTA");

            progressBar1.Maximum = 5;
            this.Show();
            progressBar1.Value = 0;
            progressBar1.Maximum = 1000;
            List<string> listaPastas = new List<string>();
            listaPastas.Add(pasta + "\\BACKUP\\");
            listaPastas.Add(pasta + "\\IMG\\");
            CriaPastasComProgressBar(listaPastas);
            listaPastas = null;
        }
        private void CriaPastasComProgressBar(List<string> pastas)
        {
            progressBar1.Visible = true;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = pastas.Count;
            progressBar1.Value = 0;
            progressBar1.Step = 0;

            foreach (string nomePasta in pastas)
            {
                if (!Directory.Exists(nomePasta))
                {
                    Directory.CreateDirectory(nomePasta);
                    // sleep de 500 milisenconds somente para mostrar o progresso em andamento
                    System.Threading.Thread.Sleep(300);
                }
            }
        }
    }
}
