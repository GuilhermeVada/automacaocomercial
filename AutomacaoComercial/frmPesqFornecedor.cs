﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using System.Data.SqlClient;
using Business;

namespace AutomacaoComercial
{
    public partial class frmPesqFornecedor : Form
    {
        public frmPesqFornecedor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frmCadFornecedor = new frmCadFornecedor();
            frmCadFornecedor.ShowDialog();
        }

        private void frmPesqFornecedor_Load(object sender, EventArgs e)
        {
            txtPesquisa.Select();
        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                DaoFornecedor fornecedor = new DaoFornecedor();
                dgvFornecedor.DataSource = fornecedor.Listagem(txtPesquisa.Text);
            }
        }

        private void dgvFornecedor_DoubleClick(object sender, EventArgs e)
        {
            if (dgvFornecedor.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela de Fornecedor digitado!");
                txtPesquisa.Focus();
            }
            else
            {
                var frmCadFornecedor = new frmCadFornecedor();
                BusinessAlteraFornecedor.idFornecedor = dgvFornecedor[0, dgvFornecedor.CurrentRow.Index].Value.ToString();
                SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
                objConn.Open();
                try
                {
                    //seleciona o campo nome da tabale "TABELA"
                    SqlCommand cmd = new SqlCommand("SELECT * FROM FORNECEDOR WHERE ID_FORNECEDOR = " + BusinessAlteraFornecedor.idFornecedor, objConn);
                    SqlDataReader objReader = cmd.ExecuteReader();
                    //se existir dados mostra no textbox TextBoxNome
                    if (objReader.Read())
                        BusinessAlteraFornecedor.razaoSocial = objReader["RAZAO_SOCIAL"].ToString();
                        BusinessAlteraFornecedor.fantasia = objReader["FANTASIA"].ToString();
                        BusinessAlteraFornecedor.inscEstadual = objReader["INSC_ESTADUAL"].ToString();
                        BusinessAlteraFornecedor.cnpj = objReader["CNPJ"].ToString();
                        BusinessAlteraFornecedor.email = objReader["EMAIL"].ToString();
                        BusinessAlteraFornecedor.telefone = objReader["TELEFONE"].ToString();
                        BusinessAlteraFornecedor.ramal = objReader["RAMAL"].ToString();
                        BusinessAlteraFornecedor.fax = objReader["FAX"].ToString();
                        BusinessAlteraFornecedor.contato = objReader["CONTATO"].ToString();
                        BusinessAlteraFornecedor.endereco = objReader["ENDERECO"].ToString();
                        BusinessAlteraFornecedor.numero = objReader["NUMERO"].ToString();
                        BusinessAlteraFornecedor.bairro = objReader["BAIRRO"].ToString();
                        BusinessAlteraFornecedor.pontoReferencia = objReader["PONTO_REF"].ToString();
                        BusinessAlteraFornecedor.complemento = objReader["COMPLEMENTO"].ToString();
                        BusinessAlteraFornecedor.cidade = objReader["CIDADE"].ToString();
                        BusinessAlteraFornecedor.uf = objReader["UF"].ToString();
                        BusinessAlteraFornecedor.cep = objReader["CEP"].ToString();
                        BusinessAlteraFornecedor.status = objReader["STATUS_SISTEMA"].ToString();
                    objReader.Close();
                }
                catch (Exception ex)
                {
                    //se ocorrer algum erro mostra uma mensagem
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    objConn.Close(); //fecha a conexão
                    objConn.Dispose(); //libera da memoria
                }
                frmCadFornecedor.ShowDialog();
            }
        }
    }
}
