﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Globalization;
using Entidades;
using Dao;
using Business;
using System.Reflection;
using Microsoft.Win32;


namespace AutomacaoComercial
{

    public partial class frmLogin : Form
    {
        private int contador = 0;
        public static string usuarioConectado;
        public static string servidorConectado;
        public frmLogin()
        {
            InitializeComponent();
            this.KeyPreview = true; // atribuição pode ser feita em 'design mode'
            this.KeyDown += new KeyEventHandler(frmLogin_KeyDown);
            //Busca a data do exe/>
            FileInfo fi = new FileInfo(Application.ExecutablePath);
            DateTime dt = fi.LastWriteTime;
            labelDateExe.Text = dt.ToString();
        }
        protected void CarregarComboBox()
        {
            SqlConnection conn = new SqlConnection(Conexao.StringDeConexao);
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT ID_ALIAS, EMPRESA, SERVIDOR, BANCO FROM ALIAS ORDER BY EMPRESA", conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.comboBox1.Items.Add(new ComboBoxItem(reader["EMPRESA"].ToString(), Convert.ToInt32(reader["ID_ALIAS"])));
                comboBox1.SelectedIndex = 0;
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            CarregarComboBox();            
            var versão = Assembly.GetExecutingAssembly().GetName().Version;
            lbVersao.Text = "Versão: " + versão.ToString();
        }
        
        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (string.IsNullOrEmpty(txtUsuario.Text))
                {
                    this.error.SetError(this.txtUsuario, "Usuário Obrigatório !");
                    txtUsuario.Focus();
                }
                else if (!string.IsNullOrEmpty(txtUsuario.Text))
                {
                    txtSenha.Focus();
                    this.error.Clear();
                }
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                try
                {
                    DaoUsuario daousuario = new DaoUsuario();
                    Usuario usuario = daousuario.BuscaLogin(txtUsuario.Text, txtSenha.Text);
                    if (usuario != null)
                    {
                        this.Hide();
                        var formPrincipal = new frmPrincipal();
                        usuarioConectado = txtUsuario.Text;
                        formPrincipal.ShowDialog();
                        this.Close();
                    }

                    else
                    {
                        if (++contador >= 3)
                        {
                            Close();
                        }
                        MessageBox.Show("Usuário ou Senha Incorreto !!");
                        txtUsuario.Clear();
                        txtSenha.Clear();
                        txtUsuario.Focus();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Banco de dados não encontrado, verifique !");
                    Application.ExitThread();
                }

            }
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 116)
            {
                this.txtUsuario.Text = "LOJA";
                this.txtSenha.Text = "LOJA";
                txtSenha.Select();
                SendKeys.Send("{ENTER}");
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtSenha_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string banco_alias;
            string servidor;
            //MessageBox.Show("O id do cara é -- > " + selectedItem.Value.ToString());
            SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
            objConn.Open();
            try
            {
                ComboBoxItem selectedItem = comboBox1.SelectedItem as ComboBoxItem;
                SqlCommand cmd = new SqlCommand("SELECT ID_ALIAS, EMPRESA, SERVIDOR, BANCO FROM ALIAS WHERE ID_ALIAS = " + selectedItem.Value.ToString(), objConn);
                SqlDataReader objReader = cmd.ExecuteReader();
                if (objReader.Read())
                {
                    servidor = objReader["SERVIDOR"].ToString();
                    banco_alias = objReader["BANCO"].ToString();
                    RegistryKey registro = Registry.CurrentUser.OpenSubKey("SISTEMA", true);
                    registro.SetValue("CAMINHO_SERVIDOR", servidor);
                    registro.SetValue("BANCO", banco_alias);
                    objReader.Close();
                    txtUsuario.Select();
                    servidorConectado = banco_alias;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                objConn.Close();
                objConn.Dispose();
            }
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_MouseClick(object sender, MouseEventArgs e)
        {

        }
    }
}
