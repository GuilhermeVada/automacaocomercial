﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmPesqAlias : Form
    {
        public frmPesqAlias()
        {
            InitializeComponent();
        }

        public void AtualizaAlias()
        {
            DaoAlias obj = new DaoAlias();
            dgvAlias.DataSource = obj.Listagem("");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frmPesqAlias = new frmCadFiliais();
            frmPesqAlias.ShowDialog();
        }

        private void frmPesqAlias_Load(object sender, EventArgs e)
        {
            AtualizaAlias();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            AtualizaAlias();
        }

        private void dgvAlias_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvAlias_DoubleClick(object sender, EventArgs e)
        {
            if (dgvAlias.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela Filiais digitado!");
            }
            else
            {
                var frmCadAlias = new frmCadFiliais();
                BusinessAlteraAlias.Codigo = dgvAlias[0, dgvAlias.CurrentRow.Index].Value.ToString();
                SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
                objConn.Open();
                try
                {
                    //seleciona o campo nome da tabale "TABELA"
                    SqlCommand cmd = new SqlCommand("SELECT * FROM ALIAS WHERE ID_ALIAS = " + BusinessAlteraAlias.Codigo, objConn);
                    SqlDataReader objReader = cmd.ExecuteReader();
                    //se existir dados mostra no textbox TextBoxNome
                    if (objReader.Read())
                        BusinessAlteraAlias.Empresa = objReader["EMPRESA"].ToString();
                    BusinessAlteraAlias.Servidor = objReader["SERVIDOR"].ToString();
                    BusinessAlteraAlias.Banco = objReader["BANCO"].ToString();
                    objReader.Close();
                }
                catch (Exception ex)
                {
                    //se ocorrer algum erro mostra uma mensagem
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    objConn.Close(); //fecha a conexão
                    objConn.Dispose(); //libera da memoria
                }
                frmCadAlias.ShowDialog();
            }
        }
    }
}