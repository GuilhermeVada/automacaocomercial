﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Business;
using System.Data.SqlClient;
using Entidades;

namespace AutomacaoComercial
{
    public partial class frmPesqCliente : Form
    {
        frmCadOrdemServico frmCadOrdem;
        public frmPesqCliente()
        {
            InitializeComponent();

        }
        public frmPesqCliente(frmCadOrdemServico frmCadOrdemServico)
        {
            InitializeComponent();
            frmCadOrdem = frmCadOrdemServico;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void AtualizaPesqCliente()
        {
            DaoCliente obj = new DaoCliente();
            dgvCliente.DataSource = obj.Cliente(txtPesquisa.Text);
        }

        public void AlteraCliente()
        {
            if (dgvCliente.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela de Clientes digitado!");
                txtPesquisa.Focus();
            }
            else
            {
                var formCadCliente = new frmCadCliente();
                BusinessAlteraCliente.Codigo = dgvCliente[0, dgvCliente.CurrentRow.Index].Value.ToString();
                SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
                objConn.Open();
                try
                {
                    //seleciona o campo nome da tabale "TABELA"
                    SqlCommand cmd = new SqlCommand("SELECT * FROM CLIENTE WHERE ID_CLIENTE = " + BusinessAlteraCliente.Codigo, objConn);
                    SqlDataReader objReader = cmd.ExecuteReader();
                    //se existir dados mostra no textbox TextBoxNome
                    if (objReader.Read())
                        BusinessAlteraCliente.Nome = objReader["NOME"].ToString();
                        BusinessAlteraCliente.Nascimento = objReader["NASCIMENTO"].ToString();
                        BusinessAlteraCliente.Rg = objReader["RG"].ToString();
                        BusinessAlteraCliente.Cpf = objReader["CPF"].ToString();
                        BusinessAlteraCliente.Naturalidade = objReader["NATURALIDADE"].ToString();
                        BusinessAlteraCliente.Sexo = objReader["SEXO"].ToString();
                        BusinessAlteraCliente.Email = objReader["EMAIL"].ToString();
                        BusinessAlteraCliente.Telefone = objReader["TELEFONE"].ToString();
                        BusinessAlteraCliente.Celular = objReader["CELULAR"].ToString();
                        BusinessAlteraCliente.Fax = objReader["FAX"].ToString();
                        BusinessAlteraCliente.EstadoCivil = objReader["ESTADO_CIVIL"].ToString();
                        BusinessAlteraCliente.Contato = objReader["CONTATO"].ToString();
                        BusinessAlteraCliente.Endereco = objReader["ENDERECO"].ToString();
                        BusinessAlteraCliente.Numero = objReader["NUMERO"].ToString();
                        BusinessAlteraCliente.Bairro = objReader["BAIRRO"].ToString();
                        BusinessAlteraCliente.PontoRef = objReader["PONTO_REF"].ToString();
                        BusinessAlteraCliente.Complemento = objReader["COMPLEMENTO"].ToString();
                        BusinessAlteraCliente.Cidade = objReader["CIDADE"].ToString();
                        BusinessAlteraCliente.UF = objReader["UF"].ToString();
                        BusinessAlteraCliente.Cep = objReader["CEP"].ToString();
                        BusinessAlteraCliente.Ativo = objReader["ATIVO"].ToString();
                        BusinessAlteraCliente.Cnpj = objReader["CNPJ"].ToString();
                        BusinessAlteraCliente.InscEstadual = objReader["INSC_ESTADUAL"].ToString();
                        BusinessAlteraCliente.Nacionalidade = objReader["NACIONALIDADE"].ToString();
                    objReader.Close();
                }
                catch (Exception ex)
                {
                    //se ocorrer algum erro mostra uma mensagem
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    objConn.Close(); //fecha a conexão
                    objConn.Dispose(); //libera da memoria
                }
                formCadCliente.ShowDialog();
            }
        }
        private void frmPesqCliente_Load(object sender, EventArgs e)
        {
            txtPesquisa.Select();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var frmCadCliente = new frmCadCliente();
            frmCadCliente.ShowDialog();
        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (string.IsNullOrEmpty(txtPesquisa.Text))
                {
                    MessageBox.Show("Por favor digite um Cliente !!");
                }
                else
                {
                    DaoCliente cliente = new DaoCliente();
                    List<ClienteVO> resultado = cliente.Cliente(txtPesquisa.Text);
                    if (resultado.Count == 0)
                    {
                        MessageBox.Show("Cliente não localizado !!");
                        txtPesquisa.Select();
                        txtPesquisa.Text = "";
                        return;
                    }
                    else
                    {
                        dgvCliente.DataSource = resultado;
                    }
                    dgvCliente.Focus();
                }
            }  
        }

        private void dgvCliente_DoubleClick(object sender, EventArgs e)
        {
            AlteraCliente();
        }
        private void dgvCliente_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dgvCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (dgvCliente.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela de Clientes digitado!");
                txtPesquisa.Focus();
            }
            else
            {
                if (Application.OpenForms.OfType<frmCadOrdemServico>().Count() > 0)
                {
                    if(e.KeyValue == 13)
                    {
                    var row = dgvCliente.SelectedRows[0];
                    var cliente = (row.DataBoundItem as ClienteVO);
                    this.Close();
                    frmCadOrdem.CarregaCliente(cliente);
                    }
                }
            }
        }

        private void dgvCliente_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            int _linhaIndice = e.RowIndex;
        }

        private void dgvCliente_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            dgvCliente.RowsDefaultCellStyle.BackColor = Color.White;
            dgvCliente.AlternatingRowsDefaultCellStyle.BackColor = Color.PapayaWhip;
        }

        private void dgvCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27)
            {
                txtPesquisa.Select();
                this.dgvCliente.DataSource = null;
                txtPesquisa.Text = "";
            }
            if(e.KeyChar == 13)
            {
                AlteraCliente();
            }
        }
    }
}