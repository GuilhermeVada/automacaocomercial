﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Entidades;
using Business;

namespace AutomacaoComercial
{
    public partial class frmCadUsuario : Form
    {
        Usuario usuario = new Usuario();
        public frmCadUsuario()
        {
            InitializeComponent();
            txtNome.Select();
        }

        public void salvar()
        {
            usuario.nome = txtNome.Text;
            usuario.telefone = txtTelefone.Text;
            usuario.celular = txtCelular.Text;
            usuario.email = txtEmail.Text;
            usuario.rg = txtRg.Text;
            usuario.cpf = txtCpf.Text;
            usuario.endereco = txtEndereco.Text;
            usuario.numero = txtNumero.Text;
            usuario.funcao = txtFuncao.Text;
            usuario.nivelSistema = txtNivelSistema.Text;
            usuario.login = txtUsuario.Text;
            usuario.senha = txtSenha.Text;
            usuario.statusSistema = txtStatus.Text;
            if (txtStatus.Text == "Ativo")
            {
                usuario.statusSistema = "A";
            }
            else
            {
                usuario.statusSistema = "I";
            }
            
            if (txtNivelSistema.Text == "Balconista")
            {
                usuario.nivelSistema = "1";
            }
            if (txtNivelSistema.Text == "Caixa")
            {
                usuario.nivelSistema = "2";
            }
            if (txtNivelSistema.Text == "Gerente")
            {
                usuario.nivelSistema = "3";
            }
            if (txtNivelSistema.Text == "Proprietário")
            {
                usuario.nivelSistema = "4";
            }
            if (txtNivelSistema.Text == "Sistema")
            {
                usuario.nivelSistema = "5";
            }
            
            new BusinessUsuario().salvar(usuario);
        }

        public void carregaDados()
        {
            txtId.Text = BusinessAlteraUsuario.Codigo.ToString();
            txtNome.Text = BusinessAlteraUsuario.Nome.ToString();
            txtTelefone.Text = BusinessAlteraUsuario.Telefone.ToString();
            txtCelular.Text = BusinessAlteraUsuario.Celular.ToString();
            txtRg.Text = BusinessAlteraUsuario.Rg.ToString();
            txtCpf.Text = BusinessAlteraUsuario.Cpf.ToString();
            txtEndereco.Text = BusinessAlteraUsuario.Endereco.ToString();
            txtNumero.Text = BusinessAlteraUsuario.Numero.ToString();
            txtFuncao.Text = BusinessAlteraUsuario.Funcao.ToString();
            txtNivelSistema.Text = BusinessAlteraUsuario.NivelSistema.ToString();
            txtEmail.Text = BusinessAlteraUsuario.Email.ToString();
            txtUsuario.Text = BusinessAlteraUsuario.Usuario.ToString();
            txtSenha.Text = BusinessAlteraUsuario.Senha.ToString();
            txtStatus.Text = BusinessAlteraUsuario.StatusSistema.ToString();
            if (BusinessAlteraUsuario.StatusSistema == "A")
            {
                txtStatus.Text = "Ativo";
            }
            else
            {
                txtStatus.Text = "Inativo";
            }
            
            if (BusinessAlteraUsuario.NivelSistema == "1")
            {
                txtNivelSistema.Text = "Balconista";
            }
            if (BusinessAlteraUsuario.NivelSistema == "2")
            {
                txtNivelSistema.Text = "Caixa";
            }
            if (BusinessAlteraUsuario.NivelSistema == "3")
            {
                txtNivelSistema.Text = "Gerente";
            }
            if (BusinessAlteraUsuario.NivelSistema == "4")
            {
                txtNivelSistema.Text = "Proprietário";
            }
            if (BusinessAlteraUsuario.NivelSistema == "5")
            {
                txtNivelSistema.Text = "Sistema";
            }
            btSalvar.Select();
        }
        public void alterar()
        {
            usuario.nome = txtNome.Text;
            usuario.telefone = txtTelefone.Text;
            usuario.celular = txtCelular.Text;
            usuario.rg = txtRg.Text;
            usuario.cpf = txtCpf.Text;
            usuario.endereco = txtEndereco.Text;
            usuario.numero = txtNumero.Text;
            usuario.funcao = txtFuncao.Text;
            usuario.nivelSistema = txtNivelSistema.Text;
            usuario.email = txtEmail.Text;
            usuario.login = txtUsuario.Text;
            usuario.senha = txtSenha.Text;
            usuario.statusSistema = txtStatus.Text;
            if (txtStatus.Text == "Ativo")
            {
                usuario.statusSistema = "A";
            }
            else
            {
                usuario.statusSistema = "I";
            }
            
            if (txtNivelSistema.Text == "Balconista")
            {
                usuario.nivelSistema = "1";
            }
            if (txtNivelSistema.Text == "Caixa")
            {
                usuario.nivelSistema = "2";
            }
            if (txtNivelSistema.Text == "Gerente")
            {
                usuario.nivelSistema = "3";
            }
            if (txtNivelSistema.Text == "Proprietário")
            {
                usuario.nivelSistema = "4";
            }
            if (txtNivelSistema.Text == "Sistema")
            {
                usuario.nivelSistema = "5";
            }
            new BusinessUsuario().Alterar(usuario);
            MessageBox.Show("Usuário alterado com sucesso !");
        }

        public void limpar()
        {
            txtNome.Text = "";
            txtTelefone.Text = "";
            txtCelular.Text = "";
            txtRg.Text = "";
            txtEmail.Text = "";
            txtEndereco.Text = "";
            txtEmail.Text = "";
            txtFuncao.Text = "";
            txtEmail.Text = "";
            txtEmail.Text = "";
            txtUsuario.Text = "";
            txtSenha.Text = "";
            txtStatus.Text = "";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmCadUsuario_Load(object sender, EventArgs e)
        {
            txtNome.Select();
            txtStatus.Text = "Ativo";
            if (txtId.Text == "")
            {
                btExcluir.Enabled = false;
            }
            if (BusinessAlteraUsuario.Codigo != null)
            {
                btExcluir.Enabled = true;
                carregaDados();
                Business.BusinessAlteraUsuario.Codigo = null;
            }
            else
            {
                limpar();
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtNome.Select();
            BusinessAlteraUsuario.Codigo = txtId.Text;
            if (string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtUsuario.Text) || string.IsNullOrEmpty(txtSenha.Text))
            {
                MessageBox.Show("O campo Nome, Usuario ou Senha estão vazios, porfavor preenche-los !");
                txtNome.Select();
                return;
            }
            else if (string.IsNullOrEmpty(txtId.Text))
            {
                salvar();
                MessageBox.Show("Usuário salvo com sucesso !");
                Close();
            }
            else
            {
                alterar();
                Close();
            }
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtTelefone.Text != "")
                {
                    txtTelefone.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCelular.Text != "")
                {
                    txtCelular.Mask = "(00)-00000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCpf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCpf.Text != "")
                {
                    txtCpf.Mask = "000,000,000-00";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtStatus_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtRg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtFuncao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtNivelSistema_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            BusinessAlteraUsuario.Codigo = txtId.Text;
            usuario.nome = txtNome.Text;
            new BusinessUsuario().Excluir(usuario);
            MessageBox.Show("O Usuário foi excluido com sucesso!");
            limpar();
            Close();
        }
    }
}