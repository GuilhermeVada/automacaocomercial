﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmPesqUsuario : Form
    {
        public frmPesqUsuario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmPesqUsuario_Load(object sender, EventArgs e)
        {
            txtPesquisa.Select();
        }
        public void AtualizaGrid()
        {
            DaoUsuario obj = new DaoUsuario();
            dgvUsuario.DataSource = obj.Listagem("");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var frmCadUsuario = new frmCadUsuario();
            frmCadUsuario.ShowDialog();
        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dgvUsuario_DoubleClick(object sender, EventArgs e)
        {
            if (dgvUsuario.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela Usuário digitado!");
            }
            else
            {
                var frmCadUsuario = new frmCadUsuario();
                BusinessAlteraUsuario.Codigo = dgvUsuario[0, dgvUsuario.CurrentRow.Index].Value.ToString();
                SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
                objConn.Open();
                try
                {
                    //seleciona o campo nome da tabale "TABELA"
                    SqlCommand cmd = new SqlCommand("SELECT * FROM USUARIO WHERE ID_USUARIO = " + BusinessAlteraUsuario.Codigo, objConn);
                    SqlDataReader objReader = cmd.ExecuteReader();
                    //se existir dados mostra no textbox TextBoxNome
                    if (objReader.Read())
                        BusinessAlteraUsuario.Nome = objReader["NOME"].ToString();
                    BusinessAlteraUsuario.Telefone = objReader["TELEFONE"].ToString();
                    BusinessAlteraUsuario.Celular = objReader["CELULAR"].ToString();
                    BusinessAlteraUsuario.Rg = objReader["RG"].ToString();
                    BusinessAlteraUsuario.Cpf = objReader["CPF"].ToString();
                    BusinessAlteraUsuario.Endereco = objReader["ENDERECO"].ToString();
                    BusinessAlteraUsuario.Numero = objReader["NUMERO"].ToString();
                    BusinessAlteraUsuario.Funcao = objReader["FUNCAO"].ToString();
                    BusinessAlteraUsuario.NivelSistema = objReader["NIVEL_SISTEMA"].ToString();
                    BusinessAlteraUsuario.Email = objReader["EMAIL"].ToString();
                    BusinessAlteraUsuario.Usuario = objReader["USUARIO"].ToString();
                    BusinessAlteraUsuario.Senha = objReader["SENHA"].ToString();
                    BusinessAlteraUsuario.StatusSistema = objReader["STATUS_SISTEMA"].ToString();

                    objReader.Close();
                }
                catch (Exception ex)
                {
                    //se ocorrer algum erro mostra uma mensagem
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    objConn.Close(); //fecha a conexão
                    objConn.Dispose(); //libera da memoria
                }
                frmCadUsuario.ShowDialog();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            AtualizaGrid();
        }
    }
}
