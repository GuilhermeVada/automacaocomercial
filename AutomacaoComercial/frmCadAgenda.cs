﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Entidades;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmCadAgenda : Form
    {
        Agenda agenda = new Agenda();
        public frmCadAgenda()
        {
            InitializeComponent();
        }

        public void salvar()
        {
            agenda.nome = txtNome.Text;
            agenda.telefone = txtTelefone.Text;
            agenda.celular = txtCelular.Text;
            agenda.fax = txtFax.Text;
            agenda.email = txtEmail.Text;
            agenda.anotacoes = txtAnotacoes.Text;
            new BusinessAgenda().salvar(agenda);
        }

        public void carregaDados()
        {
            txtId.Text = BusinessAlteraAgenda.Codigo.ToString();
            txtNome.Text = BusinessAlteraAgenda.Nome.ToString();
            txtTelefone.Text = BusinessAlteraAgenda.Telefone.ToString();
            txtCelular.Text = BusinessAlteraAgenda.Celular.ToString();
            txtFax.Text = BusinessAlteraAgenda.Fax.ToString();
            txtEmail.Text = BusinessAlteraAgenda.Email.ToString();
            txtAnotacoes.Text = BusinessAlteraAgenda.Anotacoes.ToString();
            btSalvar.Select();
        }

        public void alterar()
        {
            agenda.nome = txtNome.Text;
            agenda.telefone = txtTelefone.Text;
            agenda.celular = txtCelular.Text;
            agenda.fax = txtFax.Text;
            agenda.email = txtEmail.Text;
            agenda.anotacoes = txtAnotacoes.Text;
            new BusinessAgenda().Alterar(agenda);
            MessageBox.Show("Agenda alterado com sucesso !");
        }

        public void limpar()
        {
            txtId.Text = "";
            txtNome.Text = "";
            txtTelefone.Text = "";
            txtCelular.Text = "";
            txtFax.Text = "";
            txtEmail.Text = "";
            txtAnotacoes.Text = "";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmCadAgenda_Load(object sender, EventArgs e)
        {
            if(txtId.Text == "")
            {
                btExcluir.Enabled = false;
                txtNome.Select();
            }
            if (BusinessAlteraAgenda.Codigo != null)
            {
                btExcluir.Enabled = true;
                carregaDados();
                Business.BusinessAlteraAgenda.Codigo = null;
            }
            else
            {
                limpar();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BusinessAlteraAgenda.Codigo = txtId.Text;
            if (string.IsNullOrEmpty(txtNome.Text))
            {
                MessageBox.Show("O campo Nome está vazio, porfavor preenche-los !");
                txtNome.Select();
                return;
            }
            else if (string.IsNullOrEmpty(txtId.Text))
            {
                salvar();
                MessageBox.Show("Agenda salva com sucesso !");
                Close();
            }
            else
            {
                alterar();
                Close();
            }
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            BusinessAlteraAgenda.Codigo = txtId.Text;
            agenda.nome = txtNome.Text;
            new BusinessAgenda().Excluir(agenda);
            MessageBox.Show("O cliente foi excluido com sucesso!");
            limpar();
            Close();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtTelefone.Text != "")
                {
                    txtTelefone.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCelular.Text != "")
                {
                    txtCelular.Mask = "(00)-00000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtFax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtFax.Text != "")
                {
                    txtFax.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtAnotacoes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }
    }
}
