﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Entidades;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmCadFiliais : Form
    {
        Alias alias = new Alias();
        public frmCadFiliais()
        {
            InitializeComponent();
        }

        public void salvar()
        {
            alias.empresa = txtEmpresa.Text;
            alias.banco = txtBanco.Text;
            alias.servidor = txtServidor.Text;
            new BusinessAlias().salvar(alias);
        }
        public void alterar()
        {
            alias.empresa = txtEmpresa.Text;
            alias.banco = txtBanco.Text;
            alias.servidor = txtServidor.Text;
            new BusinessAlias().Alterar(alias);
            MessageBox.Show("Alias alterado com sucesso !");
        }
        public void carregaDados()
        {
            txtId.Text = BusinessAlteraAlias.Codigo.ToString();
            txtEmpresa.Text = BusinessAlteraAlias.Empresa.ToString();
            txtServidor.Text = BusinessAlteraAlias.Servidor.ToString();
            txtBanco.Text = BusinessAlteraAlias.Banco.ToString();
            btSalvar.Select();
        }
        public void limpar()
        {
            txtId.Text = "";
            txtEmpresa.Text = "";
            txtServidor.Text = "";
            txtBanco.Text = "";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BusinessAlteraAlias.Codigo = txtId.Text;
            if (string.IsNullOrEmpty(txtEmpresa.Text))
            {
                MessageBox.Show("O campo Empresa está vazio, porfavor preenche-los !");
                txtEmpresa.Select();
                return;
            }
            else if (string.IsNullOrEmpty(txtId.Text))
            {
                salvar();
                MessageBox.Show("Filial salva com sucesso !");
                Close();
            }
            else
            {
                alterar();
                Close();
            }
        }

        private void frmCadFiliais_Load(object sender, EventArgs e)
        {
            txtEmpresa.Select();
            if (txtId.Text == "")
            {
                btExcluir.Enabled = false;
                txtEmpresa.Select();
            }
            if (BusinessAlteraAlias.Codigo != null)
            {
                btExcluir.Enabled = true;
                carregaDados();
                Business.BusinessAlteraAlias.Codigo = null;
            }
            else
            {
                limpar();
            }
        }

        private void txtEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtBanco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtServidor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            BusinessAlteraAlias.Codigo = txtId.Text;
            alias.empresa = txtEmpresa.Text;
            new BusinessAlias().Excluir(alias);
            MessageBox.Show("O cliente foi excluido com sucesso!");
            limpar();
            Close();
        }
    }
}
