﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Microsoft.Win32;
using DFW;

namespace AutomacaoComercial
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmCadCliente = new frmPesqCliente();
            frmCadCliente.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frmPesqCliente = new frmPesqCliente();
            frmPesqCliente.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frmLogin = new frmLogin();
            frmLogin.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var frmPesqProduto = new frmPesqProduto();
            frmPesqProduto.ShowDialog();
        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmPesqProduto = new frmPesqProduto();
            frmPesqProduto.ShowDialog();
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmPesqFornecedor = new frmPesqFornecedor();
            frmPesqFornecedor.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var frmPesqAgenda = new frmPesqAgenda();
            frmPesqAgenda.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            var frmPesqOrdemServico = new frmPesqOrdemServico();
            frmPesqOrdemServico.ShowDialog();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            RegistryKey registro = Registry.CurrentUser.OpenSubKey("SISTEMA", true);
            // Retorna dados de um registro
            RegistryKey pRegKey = Registry.LocalMachine;
            pRegKey = registro;
            object caminho_pasta = pRegKey.GetValue("CAMINHO_PASTA");
            pictureBox1.ImageLocation = caminho_pasta + @"\\IMG\\FUNDO.JPG";
            object caminho_estacao = pRegKey.GetValue("ESTACAO");
            txtEstacao.Text = "Estação: " + caminho_estacao.ToString();
            //Abre a conexão com o banco de dados
            SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
            objConn.Open();
            try
            {
                //seleciona o campo nome da tabale "TABELA"
                SqlCommand cmd = new SqlCommand("SELECT * FROM CONTROLE", objConn);
                SqlDataReader objReader = cmd.ExecuteReader();
                //se existir dados mostra no textbox TextBoxNome
                if (objReader.Read())
                    this.lbEmpresa.Text = objReader["EMPRESA"].ToString();
                    this.lbTelefone.Text = objReader["TELEFONE"].ToString();
                objReader.Close();
            }
            catch (Exception ex)
            {
                //se ocorrer algum erro mostra uma mensagem
                MessageBox.Show(ex.Message);
            }
            finally
            {
                objConn.Close(); //fecha a conexão
                objConn.Dispose(); //libera da memoria
            }

            txtUsuarioConectador.Text = "Seja Bem Vindo Usuário: " + frmLogin.usuarioConectado;
            txtServidorConectado.Text = "Banco de Dados: " + frmLogin.servidorConectado;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void txtData_Click(object sender, EventArgs e)
        {

        }

        private void usuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmPesqUsuario = new frmPesqUsuario();
            frmPesqUsuario.ShowDialog();
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void aliasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmPesqAlias = new frmPesqAlias();
            frmPesqAlias.ShowDialog();
        }

        private void darumToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void backupToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var frmBackup = new frmBackup();
            frmBackup.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void lbCompromisso_Click(object sender, EventArgs e)
        {

        }
    }
}
