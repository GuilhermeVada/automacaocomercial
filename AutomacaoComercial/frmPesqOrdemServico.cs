﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmPesqOrdemServico : Form
    {
        public frmPesqOrdemServico()
        {
            InitializeComponent();
        }
        public void AtualizaOrdem()
        {
            DaoOrdem obj = new DaoOrdem();
            dgvOS.DataSource = obj.Listagem("");
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void AlteraOrdem()
        {
            if (dgvOS.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela de Ordem digitado!");
            }
            else
            {
                var formCadCliente = new frmCadCliente();
                BusinessAlteraOrdem.idOrdem = dgvOS[0, dgvOS.CurrentRow.Index].Value.ToString();
                SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
                objConn.Open();
                try
                {
                    //seleciona o campo nome da tabale "TABELA"
                    SqlCommand cmd = new SqlCommand("SELECT * FROM ORDEM_SERVICO WHERE ID_ORDEM = " + BusinessAlteraOrdem.idOrdem, objConn);
                    SqlDataReader objReader = cmd.ExecuteReader();
                    //se existir dados mostra no textbox TextBoxNome
                    if (objReader.Read())
                        BusinessAlteraOrdem.idTecnico = objReader["ID_CODTECNICO"].ToString();
                        BusinessAlteraOrdem.idCliente = objReader["ID_CODCLIENTE"].ToString();
                        BusinessAlteraOrdem.dtCadastro = objReader["DT_CADASTRO"].ToString();
                        BusinessAlteraOrdem.contato = objReader["CONTATO"].ToString();
                        BusinessAlteraOrdem.prazoEntrega = objReader["PRAZO_ENTREGA"].ToString();
                        BusinessAlteraOrdem.garantia = objReader["GARANTIA"].ToString();
                        BusinessAlteraOrdem.local = objReader["LOCAL"].ToString();
                        BusinessAlteraOrdem.defeitoReclamado = objReader["DEFEITO_RECLAMADO"].ToString();
                        BusinessAlteraOrdem.equipamentoDeixado = objReader["EQUIPAMENTO_DEIXADO"].ToString();
                        BusinessAlteraOrdem.informacoesAdc = objReader["INFORMACOES_ADC"].ToString();
                        BusinessAlteraOrdem.problemaConstatado = objReader["PROBLEMA_CONTATADO"].ToString();
                        BusinessAlteraOrdem.solucaoServico = objReader["SOLUCAO_SERVICO"].ToString();
                        BusinessAlteraOrdem.valorProduto = objReader["VALOR_PRODUTO"].ToString();
                        BusinessAlteraOrdem.valorServico = objReader["VALOR_SERVICO"].ToString();
                        BusinessAlteraOrdem.valorDeslocamento = objReader["VALOR_DESLOCAMENTO"].ToString();
                        BusinessAlteraOrdem.finalizacao = objReader["FINALIZACAO"].ToString();
                        BusinessAlteraOrdem.inicio = objReader["INICIO"].ToString();
                        BusinessAlteraOrdem.fim = objReader["FIM"].ToString();
                    objReader.Close();
                }
                catch (Exception ex)
                {
                    //se ocorrer algum erro mostra uma mensagem
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    objConn.Close(); //fecha a conexão
                    objConn.Dispose(); //libera da memoria
                }
                var frmCadOrdem = new frmCadOrdemServico();
                frmCadOrdem.ShowDialog();
            }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var frmCadOrdemServico = new frmCadOrdemServico();
            frmCadOrdemServico.ShowDialog();
        }

        private void frmPesqOrdemServico_Load(object sender, EventArgs e)
        {
            AtualizaOrdem();
        }


        private void dgvOS_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {

        }

        private void dgvOS_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = (dgvOS.CurrentCell.RowIndex);
            this.dgvOS.Rows[rowIndex].Selected = true;
            string Col1 = dgvOS[3, dgvOS.CurrentCellAddress.Y].Value.ToString();
        }

        private void dgvOS_DoubleClick(object sender, EventArgs e)
        {
            AlteraOrdem();
        }

        private void dgvOS_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            dgvOS.RowsDefaultCellStyle.BackColor = Color.White;
            dgvOS.AlternatingRowsDefaultCellStyle.BackColor = Color.PapayaWhip;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }
    }
}
