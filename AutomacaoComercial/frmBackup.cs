﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using Microsoft.Win32;

namespace AutomacaoComercial
{
    public partial class frmBackup : Form
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        public frmBackup()
        {
            InitializeComponent();
        }

        public void serverName(string str)
        {

            con = new SqlConnection("Data Source=" + str + ";Database=master;data source=.; uid=sa; pwd=@p@ssw0rd#;");
            con.Open();
            cmd = new SqlCommand("select *  from sysservers  where srvproduct='SQL Server'", con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                comboBoxServerName.Items.Add(dr[2]);
            }
            dr.Close();

        }
        public void Createconnection()
        {

            con = new SqlConnection("Data Source=" + (comboBoxServerName.Text) + "Database=master;data source=.; uid=sa; pwd=@p@ssw0rd#;");
            con.Open();
            comboBoxDataBaseName.Items.Clear();
            cmd = new SqlCommand("SELECT * FROM SYSDATABASES WHERE	name  NOT IN ('tempdb','MASTER', 'model', 'msdb', 'ReportServerTempDB', 'ReportServer')", con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                comboBoxDataBaseName.Items.Add(dr[0]);
            }

            dr.Close();

        }
        public void query(string que)
        {
            cmd = new SqlCommand(que, con);
            cmd.ExecuteNonQuery();
        }
        public void blank(string str)
        {
            RegistryKey registro = Registry.CurrentUser.OpenSubKey("SISTEMA", true);
            // Retorna dados de um registro
            RegistryKey pRegKey = Registry.LocalMachine;
            pRegKey = registro;
            object val = pRegKey.GetValue("CAMINHO_PASTA") + "BACKUP\\";
            if (string.IsNullOrEmpty(comboBoxServerName.Text) | string.IsNullOrEmpty(comboBoxDataBaseName.Text))
            {
                MessageBox.Show("Campos inválidos, por favor verifique !");
                return;
            }
            else
            {
                if (str == "backup")
                {
                    CultureInfo culture = new CultureInfo("pt-BR");
                    DateTimeFormatInfo dtfi = culture.DateTimeFormat;
                    string diasemana = culture.TextInfo.ToTitleCase(dtfi.GetDayName(DateTime.Now.DayOfWeek));
                    string data = diasemana.Substring(0,3);

                    string toRemoveAcentos = data;
                    const string StrComAcentos = "ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç'";
                    const string StrSemAcentos = "AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc ";
                    int i = 0;

                    foreach (Char c in StrComAcentos)
                    {
                        toRemoveAcentos = toRemoveAcentos.Replace(c.ToString().Trim(), StrSemAcentos[i].ToString().Trim());
                        i++;
                    }
                    saveFileDialog1.FileName = val + toRemoveAcentos.ToUpper() + "_" + comboBoxDataBaseName.Text;
                    string s = null;
                    s = saveFileDialog1.FileName;
                    query("Backup database " + comboBoxDataBaseName.Text + " to disk='" + s + "'");
                    MessageBox.Show("Backup efetuado com Sucesso!");
                    Close();
                }
            }
        }

        private void comboBoxServerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Createconnection();
        }

        private void comboBoxDataBaseName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmBackup_Load(object sender, EventArgs e)
        {
            serverName(".");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            blank("backup");
        }
    }
}
