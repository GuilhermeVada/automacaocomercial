﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmPesqAgenda : Form
    {
        public frmPesqAgenda()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmPesqAgenda_Load(object sender, EventArgs e)
        {
            txtPesquisa.Select();
        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                DaoAgenda cliente = new DaoAgenda();
                dgvAgenda.DataSource = cliente.Listagem(txtPesquisa.Text);
            }
        }

        private void dgvAgenda_DoubleClick(object sender, EventArgs e)
        {
            if (dgvAgenda.RowCount == 0)
            {
                MessageBox.Show("Não Existe Registros na Tabela Agenda digitado!");
                txtPesquisa.Focus();
            }
            else
            {
                var frmCadAgenda = new frmCadAgenda();
                BusinessAlteraAgenda.Codigo = dgvAgenda[0, dgvAgenda.CurrentRow.Index].Value.ToString();
                SqlConnection objConn = new SqlConnection(Conexao.StringDeConexao);
                objConn.Open();
                try
                {
                    //seleciona o campo nome da tabale "TABELA"
                    SqlCommand cmd = new SqlCommand("SELECT * FROM AGENDA WHERE ID_AGENDA = " + BusinessAlteraAgenda.Codigo, objConn);
                    SqlDataReader objReader = cmd.ExecuteReader();
                    //se existir dados mostra no textbox TextBoxNome
                    if (objReader.Read())
                        BusinessAlteraAgenda.Nome = objReader["NOME"].ToString();
                    BusinessAlteraAgenda.Telefone = objReader["TELEFONE"].ToString();
                    BusinessAlteraAgenda.Celular = objReader["CELULAR"].ToString();
                    BusinessAlteraAgenda.Fax = objReader["FAX"].ToString();
                    BusinessAlteraAgenda.Email = objReader["EMAIL"].ToString();
                    BusinessAlteraAgenda.Anotacoes = objReader["ANOTACOES"].ToString();
                    objReader.Close();
                }
                catch (Exception ex)
                {
                    //se ocorrer algum erro mostra uma mensagem
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    objConn.Close(); //fecha a conexão
                    objConn.Dispose(); //libera da memoria
                }
                frmCadAgenda.ShowDialog();
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var frmCadAgenda = new frmCadAgenda();
            frmCadAgenda.ShowDialog();
        }

        private void dgvAgenda_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvAgenda_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            dgvAgenda.RowsDefaultCellStyle.BackColor = Color.White;
            dgvAgenda.AlternatingRowsDefaultCellStyle.BackColor = Color.PapayaWhip;
        }
    }
}
