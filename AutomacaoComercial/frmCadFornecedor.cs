﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Entidades;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmCadFornecedor : Form
    {
        Fornecedor fornecedor = new Fornecedor();
        public frmCadFornecedor()
        {
            InitializeComponent();
        }
        public void salvar()
        {
            fornecedor.razaoSocial = txtRazao.Text;
            fornecedor.fantasia = txtFantasia.Text;
            fornecedor.inscEstadual = txtInscEstadual.Text;
            fornecedor.cnpj = txtCnpj.Text;
            fornecedor.email = txtEmail.Text;
            fornecedor.telefone = txtTelefone.Text;
            fornecedor.ramal = txtRamal.Text;
            fornecedor.fax = txtFax.Text;
            fornecedor.contato = txtContato.Text;
            fornecedor.endereco = txtEndereco.Text;
            fornecedor.numero = txtNumero.Text;
            fornecedor.bairro = txtBairro.Text;
            fornecedor.pontoReferencia = txtPontoRef.Text;
            fornecedor.complemento = txtComplemento.Text;
            fornecedor.cidade = txtCidade.Text;
            fornecedor.uf = txtEstado.Text;
            fornecedor.cep = txtCep.Text;

            if (txtStatus.Text == "Ativo")
            {
                fornecedor.status = "A";
            }
            else
            {
                fornecedor.status = "I";
            }
            fornecedor.status = fornecedor.status;
            new BusinessFornecedor().salvar(fornecedor);
        }
        public void carregaDados()
        {
            txtId.Text = BusinessAlteraFornecedor.idFornecedor.ToString();
            txtRazao.Text = BusinessAlteraFornecedor.razaoSocial.ToString();
            txtFantasia.Text = BusinessAlteraFornecedor.fantasia.ToString();
            txtInscEstadual.Text = BusinessAlteraFornecedor.inscEstadual.ToString();
            txtCnpj.Text = BusinessAlteraFornecedor.cnpj.ToString();
            txtEmail.Text = BusinessAlteraFornecedor.email.ToString();
            txtTelefone.Text = BusinessAlteraFornecedor.telefone.ToString();
            txtRamal.Text = BusinessAlteraFornecedor.ramal.ToString();
            txtFax.Text = BusinessAlteraFornecedor.fax.ToString();
            txtContato.Text = BusinessAlteraFornecedor.contato.ToString();
            txtEndereco.Text = BusinessAlteraFornecedor.endereco.ToString();
            txtNumero.Text = BusinessAlteraFornecedor.numero.ToString();
            txtBairro.Text = BusinessAlteraFornecedor.bairro.ToString();
            txtPontoRef.Text = BusinessAlteraFornecedor.pontoReferencia.ToString();
            txtComplemento.Text = BusinessAlteraFornecedor.complemento.ToString();
            txtCidade.Text = BusinessAlteraFornecedor.cidade.ToString();
            txtEstado.Text = BusinessAlteraFornecedor.uf.ToString();
            txtCep.Text = BusinessAlteraFornecedor.cep.ToString();
            if (BusinessAlteraFornecedor.status == "A")
            {
                txtStatus.Text = "Ativo";
            }
            else
            {
                txtStatus.Text = "Inativo";
            }
            btSalvar.Select();
        }
        public void limpar()
        {
            txtId.Text = "";
            txtRazao.Text = "";
            txtFantasia.Text = "";
            txtInscEstadual.Text = "";
            txtCnpj.Text = "";
            txtEmail.Text = "";
            txtTelefone.Text = "";
            txtRamal.Text = "";
            txtFax.Text = "";
            txtContato.Text = "";
            txtEndereco.Text = "";
            txtNumero.Text = "";
            txtBairro.Text = "";
            txtPontoRef.Text = "";
            txtComplemento.Text = "";
            txtCidade.Text = "";
            txtEstado.Text = "";
            txtCep.Text = "";
            btSalvar.Select();
        }
        public void alterar()
        {
            fornecedor.razaoSocial = txtRazao.Text;
            fornecedor.fantasia = txtFantasia.Text;
            fornecedor.inscEstadual = txtInscEstadual.Text;
            fornecedor.cnpj = txtCnpj.Text;
            fornecedor.email = txtEmail.Text;
            fornecedor.telefone = txtTelefone.Text;
            fornecedor.ramal = txtRamal.Text;
            fornecedor.fax = txtFax.Text;
            fornecedor.contato = txtContato.Text;
            fornecedor.endereco = txtEndereco.Text;
            fornecedor.numero = txtNumero.Text;
            fornecedor.bairro = txtBairro.Text;
            fornecedor.pontoReferencia = txtPontoRef.Text;
            fornecedor.complemento = txtComplemento.Text;
            fornecedor.cidade = txtCidade.Text;
            fornecedor.uf = txtEstado.Text;
            fornecedor.cep = txtCep.Text;
            if (txtStatus.Text == "Ativo")
            {
                fornecedor.status = "A";
            }
            else
            {
                fornecedor.status = "I";
            }
            new BusinessFornecedor().Alterar(fornecedor);
            MessageBox.Show("Fornecedor alterado com sucesso !");
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtComplemento_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmCadFornecedor_Load(object sender, EventArgs e)
        {
            txtRazao.Select();
            txtStatus.Text = "Ativo";
            if (txtId.Text == "")
            {
                btExcluir.Enabled = false;
            }
            if (BusinessAlteraFornecedor.idFornecedor != null)
            {
                btExcluir.Enabled = true;
                carregaDados();
                Business.BusinessAlteraFornecedor.idFornecedor = null;
            }
            else
            {
                limpar();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BusinessAlteraFornecedor.idFornecedor = txtId.Text;
            if (string.IsNullOrEmpty(txtRazao.Text) || (string.IsNullOrEmpty(txtStatus.Text)))
            {
                MessageBox.Show("O campo Razão Social ou o campo Status está vazio, porfavor preenche-los !");
                txtRazao.Select();
                return;
            }
            else if (string.IsNullOrEmpty(txtId.Text))
            {
                salvar();
                MessageBox.Show("Fornecedor salvo com sucesso !");
                Close();
            }
            else
            {
                alterar();
                Close();
            }
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            BusinessAlteraFornecedor.idFornecedor = txtId.Text;
            fornecedor.razaoSocial = txtRazao.Text;
            new BusinessFornecedor().Excluir(fornecedor);
            MessageBox.Show("O Fornecedoor foi excluido com sucesso!");
            limpar();
            Close();
        }

        private void txtInscEstadual_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCnpj_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCnpj.Text != "")
                {
                    txtCnpj.Mask = "00,000,000/0000-00";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtTelefone.Text != "")
                {
                    txtTelefone.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }


        private void txtFax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtFax.Text != "")
                {
                    txtFax.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCep.Text != "")
                {
                    txtCep.Mask = "00000-000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtStatus_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtRazao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtFantasia_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtRamal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtContato_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtPontoRef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtEstado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }
    }
}
