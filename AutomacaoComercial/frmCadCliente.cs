﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dao;
using Entidades;
using Business;
using System.Data.SqlClient;

namespace AutomacaoComercial
{
    public partial class frmCadCliente : Form
    {
        Cliente cliente = new Cliente();
        public frmCadCliente()
        {
            InitializeComponent();
            txtNome.Select();
        }

        public void salvar()
        {
            cliente.nome = txtNome.Text;
            cliente.nascimento = txtNascimento.Text;
            cliente.rg = txtRg.Text;
            cliente.cpf = txtCpf.Text;
            cliente.naturalidade = txtNaturalidade.Text;
            cliente.sexo = txtSexo.Text;
            cliente.endereco = txtEndereco.Text;
            cliente.numero = txtNumero.Text;
            cliente.complemento = txtComplemento.Text;
            cliente.bairro = txtBairro.Text;
            cliente.pontoReferencia = txtPontoRef.Text;
            cliente.cidade = txtCidade.Text;
            cliente.uf = txtEstado.Text;
            cliente.cep = txtCep.Text;
            cliente.telefone = txtTelefone.Text;
            cliente.celular = txtCelular.Text;
            cliente.fax = txtFax.Text;
            cliente.email = txtEmail.Text;
            cliente.estadoCivil = txtEstadoCivil.Text;
            cliente.contato = txtContato.Text;
            cliente.cnpj = txtCnpj.Text;
            cliente.inscEstadual = txtInscEst.Text;
            cliente.nacionalidade = txtNacionalidade.Text;
            if (txtStatus.Text == "Ativo")
            {
                cliente.ativo = "A";
            }
            else
            {
                cliente.ativo = "I";
            }
            cliente.ativo = cliente.ativo;
            new BusinessCliente().salvar(cliente);
        }

        public void alterar()
        {
            cliente.nome = txtNome.Text;
            cliente.nascimento = txtNascimento.Text;
            cliente.rg = txtRg.Text;
            cliente.cpf = txtCpf.Text;
            cliente.naturalidade = txtNaturalidade.Text;
            cliente.sexo = txtSexo.Text;
            cliente.endereco = txtEndereco.Text;
            cliente.numero = txtNumero.Text;
            cliente.complemento = txtComplemento.Text;
            cliente.bairro = txtBairro.Text;
            cliente.pontoReferencia = txtPontoRef.Text;
            cliente.cidade = txtCidade.Text;
            cliente.uf = txtEstado.Text;
            cliente.cep = txtCep.Text;
            cliente.telefone = txtTelefone.Text;
            cliente.celular = txtCelular.Text;
            cliente.fax = txtFax.Text;
            cliente.email = txtEmail.Text;
            cliente.estadoCivil = txtEstadoCivil.Text;
            cliente.contato = txtContato.Text;
            cliente.ativo = txtStatus.Text;
            cliente.cnpj = txtCnpj.Text;
            cliente.inscEstadual = txtInscEst.Text;
            cliente.nacionalidade = txtNacionalidade.Text;
            if (txtStatus.Text == "Ativo")
            {
                cliente.ativo = "A";
            }
            else
            {
                cliente.ativo = "I";
            }

            new BusinessCliente().Alterar(cliente);
            MessageBox.Show("Cliente alterado com sucesso !");
        }

        public void carregaDados()
        {
            txtId.Text = BusinessAlteraCliente.Codigo.ToString();
            txtNome.Text = BusinessAlteraCliente.Nome.ToString();
            txtNascimento.Text = BusinessAlteraCliente.Nascimento.ToString();
            txtRg.Text = BusinessAlteraCliente.Rg.ToString();
            txtCpf.Text = BusinessAlteraCliente.Cpf.ToString();
            txtNaturalidade.Text = BusinessAlteraCliente.Naturalidade.ToString();
            txtSexo.Text = BusinessAlteraCliente.Sexo.ToString();
            txtEndereco.Text = BusinessAlteraCliente.Endereco.ToString();
            txtNumero.Text = BusinessAlteraCliente.Numero.ToString();
            txtComplemento.Text = BusinessAlteraCliente.Complemento.ToString();
            txtBairro.Text = BusinessAlteraCliente.Bairro.ToString();
            txtPontoRef.Text = BusinessAlteraCliente.PontoRef.ToString();
            txtCidade.Text = BusinessAlteraCliente.Cidade.ToString();
            txtEstado.Text = BusinessAlteraCliente.UF.ToString();
            txtCep.Text = BusinessAlteraCliente.Cep.ToString();
            txtTelefone.Text = BusinessAlteraCliente.Telefone.ToString();
            txtCelular.Text = BusinessAlteraCliente.Celular.ToString();
            txtFax.Text = BusinessAlteraCliente.Fax.ToString();
            txtEmail.Text = BusinessAlteraCliente.Email.ToString();
            txtEstadoCivil.Text = BusinessAlteraCliente.EstadoCivil.ToString();
            txtContato.Text = BusinessAlteraCliente.Contato.ToString();
            txtStatus.Text = BusinessAlteraCliente.Ativo.ToString();
            txtCnpj.Text = BusinessAlteraCliente.Cnpj.ToString();
            txtInscEst.Text = BusinessAlteraCliente.InscEstadual.ToString();
            txtNacionalidade.Text = BusinessAlteraCliente.Nacionalidade.ToString();
            if (BusinessAlteraCliente.Ativo == "A")
            {
                txtStatus.Text = "Ativo";
            }
            else
            {
                txtStatus.Text = "Inativo";
            }
            txtStatus.Select();
        }

        public void limpar()
        {
            txtId.Text = "";
            txtNome.Text = "";
            txtNascimento.Text = "";
            txtRg.Text = "";
            txtCpf.Text = "";
            txtNaturalidade.Text = "";
            txtSexo.Text = "";
            txtEndereco.Text = "";
            txtNumero.Text = "";
            txtComplemento.Text = "";
            txtBairro.Text = "";
            txtCidade.Text = "";
            txtEstado.Text = "";
            txtCep.Text = "";
            txtTelefone.Text = "";
            txtCelular.Text = "";
            txtFax.Text = "";
            txtEmail.Text = "";
            txtContato.Text = "";
            txtEstadoCivil.Text = "";
            txtPontoRef.Text = "";
            txtStatus.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmCadCliente_Load(object sender, EventArgs e)
        {
            txtNome.Select();
            txtStatus.Text = "Ativo";
            if (txtId.Text == "")
            {
                btExcluir.Enabled = false;
            }
            if (BusinessAlteraCliente.Codigo != null)
            {
                btExcluir.Enabled = true;
                carregaDados();
                Business.BusinessAlteraCliente.Codigo = null;
            }
            else
            {
                limpar();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BusinessAlteraCliente.Codigo = txtId.Text;
            if (string.IsNullOrEmpty(txtNome.Text) || (string.IsNullOrEmpty(txtStatus.Text)))
            {
                MessageBox.Show("O campo Nome ou o campo Status está vazio, porfavor preenche-los !");
                txtNome.Select();
                return;
            }
            else if (string.IsNullOrEmpty(txtId.Text))
            {
                salvar();
                MessageBox.Show("Cliente salvo com sucesso !");
                Close();
            }
            else
            {
                alterar();
                Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BusinessAlteraCliente.Codigo = txtId.Text;
            cliente.nome = txtNome.Text;
            new BusinessCliente().Excluir(cliente);
            MessageBox.Show("O cliente foi excluido com sucesso!");
            limpar();
            Close();
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtTelefone.Text != "")
                {
                    txtTelefone.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCelular.Text != "")
                {
                    txtCelular.Mask = "(00)-00000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtFax_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtFax.Text != "")
                {
                    txtFax.Mask = "(00)-0000-0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCep.Text != "")
                {
                    txtCep.Mask = "00000-000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtNascimento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtNascimento.Text != "")
                {
                    txtNascimento.Mask = "00/00/0000";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtRg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtCpf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCpf.Text != "")
                {
                    txtCpf.Mask = "000,000,000-00";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void txtNaturalidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtSexo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtContato_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEndereco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtNumero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtBairro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtPontoRef_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtCidade_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEstado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEstadoCivil_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtStatus_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtInscEst_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtCnpj_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtCnpj.Text != "")
                {
                    txtCnpj.Mask = "00,000,000/0000-00";
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
                else
                {
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }
    }
}