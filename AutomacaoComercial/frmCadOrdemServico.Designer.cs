﻿namespace AutomacaoComercial
{
    partial class frmCadOrdemServico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadOrdemServico));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.panel5 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTecResponsavel = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIdOS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtIdCliente = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtContato = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.txtLocal = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtGarantia = new System.Windows.Forms.ComboBox();
            this.txtDataExecucao = new System.Windows.Forms.DateTimePicker();
            this.txtDataEmissao = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtEquipamento = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDefeito = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtInfAdicionais = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtProblema = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtSolucao = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtFinalizacaoOrdem = new System.Windows.Forms.MaskedTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtFim = new System.Windows.Forms.MaskedTextBox();
            this.txtInicio = new System.Windows.Forms.MaskedTextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtProdutos = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtValorTotal = new System.Windows.Forms.TextBox();
            this.txtAcrescimo = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtDeslocamento = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtServico = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.stiReport2 = new Stimulsoft.Report.StiReport();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Location = new System.Drawing.Point(0, 671);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(975, 25);
            this.toolStrip1.TabIndex = 32;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.button5);
            this.panel5.Controls.Add(this.button3);
            this.panel5.Controls.Add(this.button1);
            this.panel5.Controls.Add(this.button2);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.button4);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(972, 74);
            this.panel5.TabIndex = 51;
            // 
            // button5
            // 
            this.button5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button5.BackColor = System.Drawing.SystemColors.Window;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(836, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(58, 63);
            this.button5.TabIndex = 36;
            this.button5.Text = "&Excluir";
            this.button5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button3.BackColor = System.Drawing.SystemColors.Window;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(641, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(66, 63);
            this.button3.TabIndex = 32;
            this.button3.Text = "&Imprimir";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button1.BackColor = System.Drawing.SystemColors.Window;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(776, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 63);
            this.button1.TabIndex = 34;
            this.button1.Text = "&Limpar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button2.BackColor = System.Drawing.SystemColors.Window;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(713, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(57, 63);
            this.button2.TabIndex = 33;
            this.button2.Text = "Sal&var";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(78, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(424, 32);
            this.label7.TabIndex = 6;
            this.label7.Text = "Cadastro de ordem de serviço";
            // 
            // button4
            // 
            this.button4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.button4.BackColor = System.Drawing.SystemColors.Window;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(899, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(58, 63);
            this.button4.TabIndex = 35;
            this.button4.Text = "&Sair";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 69);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(403, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 16);
            this.label1.TabIndex = 52;
            this.label1.Text = "Informação da Ordem de Serviço";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.txtTecResponsavel);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtIdOS);
            this.panel1.Location = new System.Drawing.Point(0, 94);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(483, 40);
            this.panel1.TabIndex = 100;
            // 
            // txtTecResponsavel
            // 
            this.txtTecResponsavel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTecResponsavel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtTecResponsavel.FormattingEnabled = true;
            this.txtTecResponsavel.Location = new System.Drawing.Point(296, 7);
            this.txtTecResponsavel.Name = "txtTecResponsavel";
            this.txtTecResponsavel.Size = new System.Drawing.Size(180, 21);
            this.txtTecResponsavel.TabIndex = 2;
            this.txtTecResponsavel.SelectedIndexChanged += new System.EventHandler(this.txtTecResponsavel_SelectedIndexChanged);
            this.txtTecResponsavel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTecResponsavel_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(169, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Técnico Responsável";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nº da Ordem";
            // 
            // txtIdOS
            // 
            this.txtIdOS.Enabled = false;
            this.txtIdOS.Location = new System.Drawing.Point(87, 7);
            this.txtIdOS.Name = "txtIdOS";
            this.txtIdOS.Size = new System.Drawing.Size(76, 21);
            this.txtIdOS.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(148, 135);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 16);
            this.label4.TabIndex = 54;
            this.label4.Text = "Cliente da Ordem de Serviço";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.txtCidade);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.txtBairro);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.txtNumero);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.txtEndereco);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtNome);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtIdCliente);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(1, 153);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(482, 93);
            this.panel2.TabIndex = 101;
            // 
            // txtCidade
            // 
            this.txtCidade.Enabled = false;
            this.txtCidade.Location = new System.Drawing.Point(282, 61);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(189, 21);
            this.txtCidade.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(237, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Cidade";
            // 
            // txtBairro
            // 
            this.txtBairro.Enabled = false;
            this.txtBairro.Location = new System.Drawing.Point(65, 61);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(168, 21);
            this.txtBairro.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 64);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Bairro";
            // 
            // txtNumero
            // 
            this.txtNumero.Enabled = false;
            this.txtNumero.Location = new System.Drawing.Point(417, 35);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(54, 21);
            this.txtNumero.TabIndex = 6;
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(364, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Número";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Enabled = false;
            this.txtEndereco.Location = new System.Drawing.Point(65, 35);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(292, 21);
            this.txtEndereco.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Endereço";
            // 
            // txtNome
            // 
            this.txtNome.Enabled = false;
            this.txtNome.Location = new System.Drawing.Point(156, 8);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(314, 21);
            this.txtNome.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(116, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Nome";
            // 
            // txtIdCliente
            // 
            this.txtIdCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtIdCliente.Location = new System.Drawing.Point(65, 8);
            this.txtIdCliente.Name = "txtIdCliente";
            this.txtIdCliente.Size = new System.Drawing.Size(50, 21);
            this.txtIdCliente.TabIndex = 3;
            this.txtIdCliente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtIdCliente_KeyDown);
            this.txtIdCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIdCliente_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Código";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Blue;
            this.label12.Location = new System.Drawing.Point(154, 247);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(208, 16);
            this.label12.TabIndex = 56;
            this.label12.Text = "Dados Adicionais do Cliente";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.txtCelular);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.txtTelefone);
            this.panel4.Controls.Add(this.txtContato);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Location = new System.Drawing.Point(0, 265);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(483, 40);
            this.panel4.TabIndex = 103;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(323, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "Celular";
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(370, 6);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(106, 21);
            this.txtCelular.TabIndex = 11;
            this.txtCelular.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCelular_KeyDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(169, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Telefone";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(225, 7);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(98, 21);
            this.txtTelefone.TabIndex = 10;
            this.txtTelefone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTelefone_KeyDown);
            // 
            // txtContato
            // 
            this.txtContato.Location = new System.Drawing.Point(61, 7);
            this.txtContato.Name = "txtContato";
            this.txtContato.Size = new System.Drawing.Size(108, 21);
            this.txtContato.TabIndex = 9;
            this.txtContato.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtContato_KeyDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Contato";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Blue;
            this.label37.Location = new System.Drawing.Point(7, 306);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(174, 16);
            this.label37.TabIndex = 57;
            this.label37.Text = "Informação Adicionais ";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.txtLocal);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.txtGarantia);
            this.panel3.Controls.Add(this.txtDataExecucao);
            this.panel3.Controls.Add(this.txtDataEmissao);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Location = new System.Drawing.Point(489, 94);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(483, 67);
            this.panel3.TabIndex = 104;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(196, 38);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 13);
            this.label22.TabIndex = 23;
            this.label22.Text = "Local";
            // 
            // txtLocal
            // 
            this.txtLocal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtLocal.FormattingEnabled = true;
            this.txtLocal.Items.AddRange(new object[] {
            "Empresa",
            "Interno",
            "Residêncial"});
            this.txtLocal.Location = new System.Drawing.Point(234, 34);
            this.txtLocal.Name = "txtLocal";
            this.txtLocal.Size = new System.Drawing.Size(201, 21);
            this.txtLocal.TabIndex = 18;
            this.txtLocal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLocal_KeyDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(73, 38);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(56, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "Garantia";
            // 
            // txtGarantia
            // 
            this.txtGarantia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txtGarantia.FormattingEnabled = true;
            this.txtGarantia.Items.AddRange(new object[] {
            "Sim",
            "Não"});
            this.txtGarantia.Location = new System.Drawing.Point(130, 34);
            this.txtGarantia.Name = "txtGarantia";
            this.txtGarantia.Size = new System.Drawing.Size(54, 21);
            this.txtGarantia.TabIndex = 17;
            this.txtGarantia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtGarantia_KeyDown);
            // 
            // txtDataExecucao
            // 
            this.txtDataExecucao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDataExecucao.Location = new System.Drawing.Point(346, 8);
            this.txtDataExecucao.Name = "txtDataExecucao";
            this.txtDataExecucao.Size = new System.Drawing.Size(89, 21);
            this.txtDataExecucao.TabIndex = 16;
            this.txtDataExecucao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDataExecucao_KeyDown);
            // 
            // txtDataEmissao
            // 
            this.txtDataEmissao.Enabled = false;
            this.txtDataEmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDataEmissao.Location = new System.Drawing.Point(130, 8);
            this.txtDataEmissao.Name = "txtDataEmissao";
            this.txtDataEmissao.Size = new System.Drawing.Size(89, 21);
            this.txtDataEmissao.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(231, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(115, 13);
            this.label17.TabIndex = 13;
            this.label17.Text = "Prazo de Execução";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Data de Emissão";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(648, 161);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(214, 16);
            this.label23.TabIndex = 105;
            this.label23.Text = "Informação do Equipamento";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.txtEquipamento);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.txtDefeito);
            this.panel6.Location = new System.Drawing.Point(489, 180);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(483, 257);
            this.panel6.TabIndex = 105;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(252, 127);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(156, 13);
            this.label29.TabIndex = 34;
            this.label29.Text = "Quantidade de Caracteres";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Blue;
            this.label26.Location = new System.Drawing.Point(7, 125);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(175, 16);
            this.label26.TabIndex = 33;
            this.label26.Text = "Equipamentos Deixado";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(407, 127);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 13);
            this.label27.TabIndex = 32;
            this.label27.Text = "0 de 1000";
            // 
            // txtEquipamento
            // 
            this.txtEquipamento.Location = new System.Drawing.Point(4, 143);
            this.txtEquipamento.Multiline = true;
            this.txtEquipamento.Name = "txtEquipamento";
            this.txtEquipamento.Size = new System.Drawing.Size(468, 103);
            this.txtEquipamento.TabIndex = 20;
            this.txtEquipamento.TextChanged += new System.EventHandler(this.txtEquipamento_TextChanged);
            this.txtEquipamento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEquipamento_KeyDown);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(251, 7);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(156, 13);
            this.label28.TabIndex = 30;
            this.label28.Text = "Quantidade de Caracteres";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Blue;
            this.label25.Location = new System.Drawing.Point(7, 6);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(144, 16);
            this.label25.TabIndex = 29;
            this.label25.Text = "Defeito Reclamado";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(406, 7);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 27;
            this.label24.Text = "0 de 1000";
            // 
            // txtDefeito
            // 
            this.txtDefeito.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtDefeito.Location = new System.Drawing.Point(4, 23);
            this.txtDefeito.Multiline = true;
            this.txtDefeito.Name = "txtDefeito";
            this.txtDefeito.Size = new System.Drawing.Size(467, 99);
            this.txtDefeito.TabIndex = 19;
            this.txtDefeito.TextChanged += new System.EventHandler(this.txtDefeito_TextChanged);
            this.txtDefeito.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDefeito_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.txtInfAdicionais);
            this.panel7.Location = new System.Drawing.Point(0, 325);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(483, 112);
            this.panel7.TabIndex = 106;
            // 
            // txtInfAdicionais
            // 
            this.txtInfAdicionais.Location = new System.Drawing.Point(8, 6);
            this.txtInfAdicionais.Multiline = true;
            this.txtInfAdicionais.Name = "txtInfAdicionais";
            this.txtInfAdicionais.Size = new System.Drawing.Size(468, 95);
            this.txtInfAdicionais.TabIndex = 21;
            this.txtInfAdicionais.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtInfAdicionais.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInfAdicionais_KeyDown);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Blue;
            this.label30.Location = new System.Drawing.Point(373, 438);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(220, 16);
            this.label30.TabIndex = 107;
            this.label30.Text = "Solução da Ordem de Serviço";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.txtProblema);
            this.panel8.Controls.Add(this.label31);
            this.panel8.Controls.Add(this.label33);
            this.panel8.Controls.Add(this.label34);
            this.panel8.Controls.Add(this.txtSolucao);
            this.panel8.Controls.Add(this.label32);
            this.panel8.Controls.Add(this.label36);
            this.panel8.Controls.Add(this.label35);
            this.panel8.Location = new System.Drawing.Point(1, 457);
            this.panel8.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(484, 209);
            this.panel8.TabIndex = 107;
            // 
            // txtProblema
            // 
            this.txtProblema.Location = new System.Drawing.Point(7, 21);
            this.txtProblema.Multiline = true;
            this.txtProblema.Name = "txtProblema";
            this.txtProblema.Size = new System.Drawing.Size(467, 74);
            this.txtProblema.TabIndex = 22;
            this.txtProblema.TextChanged += new System.EventHandler(this.txtProblema_TextChanged);
            this.txtProblema.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProblema_KeyDown);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(251, 100);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(156, 13);
            this.label31.TabIndex = 38;
            this.label31.Text = "Quantidade de Caracteres";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(406, 100);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(64, 13);
            this.label33.TabIndex = 36;
            this.label33.Text = "0 de 1000";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Blue;
            this.label34.Location = new System.Drawing.Point(7, 99);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(220, 16);
            this.label34.TabIndex = 37;
            this.label34.Text = "Solução da Ordem de Serviço";
            // 
            // txtSolucao
            // 
            this.txtSolucao.Location = new System.Drawing.Point(7, 116);
            this.txtSolucao.Multiline = true;
            this.txtSolucao.Name = "txtSolucao";
            this.txtSolucao.Size = new System.Drawing.Size(466, 74);
            this.txtSolucao.TabIndex = 23;
            this.txtSolucao.TextChanged += new System.EventHandler(this.txtSolucao_TextChanged);
            this.txtSolucao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSolucao_KeyDown);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(251, 5);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(156, 13);
            this.label32.TabIndex = 34;
            this.label32.Text = "Quantidade de Caracteres";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(406, 5);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(64, 13);
            this.label36.TabIndex = 29;
            this.label36.Text = "0 de 1000";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Blue;
            this.label35.Location = new System.Drawing.Point(7, 4);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(165, 16);
            this.label35.TabIndex = 30;
            this.label35.Text = "Problema Constatado";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.tabControl1);
            this.panel9.Location = new System.Drawing.Point(489, 457);
            this.panel9.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(485, 209);
            this.panel9.TabIndex = 108;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(6, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(475, 196);
            this.tabControl1.TabIndex = 106;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.tabPage1.Controls.Add(this.txtFinalizacaoOrdem);
            this.tabPage1.Controls.Add(this.label47);
            this.tabPage1.Controls.Add(this.label49);
            this.tabPage1.Controls.Add(this.label50);
            this.tabPage1.Controls.Add(this.label48);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.txtFim);
            this.tabPage1.Controls.Add(this.txtInicio);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.txtProdutos);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.txtValorTotal);
            this.tabPage1.Controls.Add(this.txtAcrescimo);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.txtDesconto);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.txtDeslocamento);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.txtServico);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(467, 170);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Orçamento/Finalização";
            // 
            // txtFinalizacaoOrdem
            // 
            this.txtFinalizacaoOrdem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFinalizacaoOrdem.Location = new System.Drawing.Point(155, 137);
            this.txtFinalizacaoOrdem.Name = "txtFinalizacaoOrdem";
            this.txtFinalizacaoOrdem.Size = new System.Drawing.Size(77, 21);
            this.txtFinalizacaoOrdem.TabIndex = 30;
            this.txtFinalizacaoOrdem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFinalizacaoOrdem_KeyDown);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(166, 110);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(350, 13);
            this.label47.TabIndex = 41;
            this.label47.Text = "_________________________________________________";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Enabled = false;
            this.label49.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Blue;
            this.label49.Location = new System.Drawing.Point(8, 10);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(121, 16);
            this.label49.TabIndex = 39;
            this.label49.Text = "Valor da Ordem";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Enabled = false;
            this.label50.Location = new System.Drawing.Point(121, 7);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(350, 13);
            this.label50.TabIndex = 40;
            this.label50.Text = "_________________________________________________";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Blue;
            this.label48.Location = new System.Drawing.Point(4, 113);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(165, 16);
            this.label48.TabIndex = 25;
            this.label48.Text = "Finalização da Ordem";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(314, 140);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(27, 13);
            this.label46.TabIndex = 37;
            this.label46.Text = "Fim";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(233, 140);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(38, 13);
            this.label45.TabIndex = 36;
            this.label45.Text = "Início";
            // 
            // txtFim
            // 
            this.txtFim.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtFim.Location = new System.Drawing.Point(342, 137);
            this.txtFim.Name = "txtFim";
            this.txtFim.Size = new System.Drawing.Size(43, 21);
            this.txtFim.TabIndex = 32;
            this.txtFim.ValidatingType = typeof(System.DateTime);
            this.txtFim.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFim_KeyDown);
            // 
            // txtInicio
            // 
            this.txtInicio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtInicio.Location = new System.Drawing.Point(271, 137);
            this.txtInicio.Name = "txtInicio";
            this.txtInicio.Size = new System.Drawing.Size(43, 21);
            this.txtInicio.TabIndex = 31;
            this.txtInicio.ValidatingType = typeof(System.DateTime);
            this.txtInicio.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInicio_KeyDown);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(24, 140);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(130, 13);
            this.label44.TabIndex = 22;
            this.label44.Text = "Finalização da Ordem";
            // 
            // txtProdutos
            // 
            this.txtProdutos.Enabled = false;
            this.txtProdutos.Location = new System.Drawing.Point(145, 23);
            this.txtProdutos.Name = "txtProdutos";
            this.txtProdutos.Size = new System.Drawing.Size(134, 21);
            this.txtProdutos.TabIndex = 24;
            this.txtProdutos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProdutos_KeyDown);
            this.txtProdutos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtProdutos_KeyPress);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Enabled = false;
            this.label43.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(244, 87);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(78, 13);
            this.label43.TabIndex = 32;
            this.label43.Text = "Valor Total";
            // 
            // txtValorTotal
            // 
            this.txtValorTotal.Enabled = false;
            this.txtValorTotal.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorTotal.Location = new System.Drawing.Point(326, 81);
            this.txtValorTotal.Name = "txtValorTotal";
            this.txtValorTotal.Size = new System.Drawing.Size(133, 27);
            this.txtValorTotal.TabIndex = 29;
            // 
            // txtAcrescimo
            // 
            this.txtAcrescimo.Enabled = false;
            this.txtAcrescimo.Location = new System.Drawing.Point(371, 49);
            this.txtAcrescimo.Name = "txtAcrescimo";
            this.txtAcrescimo.Size = new System.Drawing.Size(90, 21);
            this.txtAcrescimo.TabIndex = 28;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Enabled = false;
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(283, 52);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(89, 13);
            this.label42.TabIndex = 29;
            this.label42.Text = "(+) Acréscimo";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Enabled = false;
            this.label41.ForeColor = System.Drawing.Color.Green;
            this.label41.Location = new System.Drawing.Point(292, 27);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(78, 13);
            this.label41.TabIndex = 28;
            this.label41.Text = "(-) Desconto";
            // 
            // txtDesconto
            // 
            this.txtDesconto.Enabled = false;
            this.txtDesconto.Location = new System.Drawing.Point(371, 23);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(91, 21);
            this.txtDesconto.TabIndex = 27;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Enabled = false;
            this.label40.Location = new System.Drawing.Point(5, 76);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(139, 13);
            this.label40.TabIndex = 26;
            this.label40.Text = "Valor do Deslocamento";
            // 
            // txtDeslocamento
            // 
            this.txtDeslocamento.Enabled = false;
            this.txtDeslocamento.Location = new System.Drawing.Point(146, 73);
            this.txtDeslocamento.Name = "txtDeslocamento";
            this.txtDeslocamento.Size = new System.Drawing.Size(90, 21);
            this.txtDeslocamento.TabIndex = 26;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Enabled = false;
            this.label39.Location = new System.Drawing.Point(42, 52);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(101, 13);
            this.label39.TabIndex = 24;
            this.label39.Text = "Valor do Serviço";
            // 
            // txtServico
            // 
            this.txtServico.Enabled = false;
            this.txtServico.Location = new System.Drawing.Point(146, 49);
            this.txtServico.Name = "txtServico";
            this.txtServico.Size = new System.Drawing.Size(133, 21);
            this.txtServico.TabIndex = 25;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Enabled = false;
            this.label38.Location = new System.Drawing.Point(35, 28);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(109, 13);
            this.label38.TabIndex = 22;
            this.label38.Text = "Valor do Produtos";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(254, 310);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(156, 13);
            this.label18.TabIndex = 40;
            this.label18.Text = "Quantidade de Caracteres";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(409, 310);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 13);
            this.label19.TabIndex = 39;
            this.label19.Text = "0 de 1000";
            // 
            // stiReport2
            // 
            this.stiReport2.EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV2;
            this.stiReport2.ReferencedAssemblies = new string[] {
        "System.Dll",
        "System.Drawing.Dll",
        "System.Windows.Forms.Dll",
        "System.Data.Dll",
        "System.Xml.Dll",
        "Stimulsoft.Controls.Dll",
        "Stimulsoft.Base.Dll",
        "Stimulsoft.Report.Dll"};
            this.stiReport2.ReportAlias = "Relatório";
            this.stiReport2.ReportGuid = "c3c74ac1dd0543abbca9cc8d5da3b027";
            this.stiReport2.ReportName = "Relatório";
            this.stiReport2.ReportSource = resources.GetString("stiReport2.ReportSource");
            this.stiReport2.ReportUnit = Stimulsoft.Report.StiReportUnitType.Centimeters;
            this.stiReport2.ScriptLanguage = Stimulsoft.Report.StiReportLanguageType.CSharp;
            // 
            // frmCadOrdemServico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 696);
            this.ControlBox = false;
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadOrdemServico";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulário de Cadastro de Ordem de Serviço";
            this.Load += new System.EventHandler(this.frmCadOrdemServico_Load);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIdOS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox txtTecResponsavel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIdCliente;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtContato;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker txtDataEmissao;
        private System.Windows.Forms.DateTimePicker txtDataExecucao;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox txtGarantia;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox txtLocal;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtDefeito;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtEquipamento;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtInfAdicionais;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox txtProblema;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtSolucao;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.MaskedTextBox txtFim;
        private System.Windows.Forms.MaskedTextBox txtInicio;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtProdutos;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtValorTotal;
        private System.Windows.Forms.TextBox txtAcrescimo;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtDeslocamento;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtServico;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.MaskedTextBox txtFinalizacaoOrdem;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private Stimulsoft.Report.StiReport stiReport2;
        private System.Windows.Forms.Button button5;
    }
}