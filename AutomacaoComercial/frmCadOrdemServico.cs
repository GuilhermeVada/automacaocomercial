﻿using Dao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Business;

namespace AutomacaoComercial
{
    public partial class frmCadOrdemServico : Form
    {
        ClienteVO cliente = new ClienteVO();
        Ordem ordem = new Ordem();
        public frmCadOrdemServico(int Cliente_ID)
        {
            InitializeComponent();
            BusinessOrdem ordem = new BusinessOrdem();
            cliente = ordem.BuscaClienteID(Cliente_ID);
            CarregaCliente(cliente);
        }
        public frmCadOrdemServico()
        {
            InitializeComponent();
        }

        public void limpar()
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }
        protected void CarregarComboBox()
        {
            SqlConnection conn = new SqlConnection(Conexao.StringDeConexao);
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT	* FROM	USUARIO WHERE FUNCAO LIKE '%TECNICO%'", conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                this.txtTecResponsavel.Items.Add(new ComboBoxItem(reader["NOME"].ToString(), Convert.ToInt32(reader["ID_USUARIO"])));
                txtTecResponsavel.SelectedIndex = 0;
            }
        }
        public void CarregaCliente(ClienteVO cliente)
        {
            txtIdCliente.Text = Convert.ToInt32(cliente.idCliente).ToString();
            txtNome.Text = cliente.Nome.ToString();
            txtEndereco.Text = cliente.Endereco.ToString();
            txtNumero.Text = cliente.Numero.ToString();
            txtBairro.Text = cliente.Bairro.ToString();
            txtCidade.Text = cliente.Cidade.ToString();
            txtContato.Text = cliente.Contato.ToString();
            txtTelefone.Text = cliente.Telefone.ToString();
            txtCelular.Text = cliente.Celular.ToString();
        }
        public void salvar()
        {
            Ordem ordem = new Ordem();
            ComboBoxItem selectedItem = txtTecResponsavel.SelectedItem as ComboBoxItem;
            ordem.idTecnico = Convert.ToInt32(selectedItem.Value.ToString());
            ordem.idCliente = Convert.ToInt32(txtIdCliente.Text);
            ordem.contato = txtContato.Text;
            ordem.dtCadastro = txtDataEmissao.Text;
            ordem.prazoEntrega = txtDataExecucao.Text;
            //ordem.garantia = txtGarantia.Text;
            ordem.local = txtLocal.Text;
            ordem.defeitoReclamado = txtDefeito.Text;
            ordem.equipamentoDeixado = txtEquipamento.Text;
            ordem.informacoesAdc = txtInfAdicionais.Text;
            ordem.problemaConstatado = txtProblema.Text;
            ordem.solucaoServico = txtServico.Text;
            ordem.valorProduto = txtProdutos.Text;
            ordem.valorServico = txtServico.Text;
            ordem.valorDeslocamento = txtDeslocamento.Text;
            ordem.totalOrdem = txtValorTotal.Text;
            ordem.finalizacao = txtFinalizacaoOrdem.Text;
            ordem.inicio = txtInicio.Text;
            ordem.fim = txtFim.Text;
            if(txtGarantia.Text == "Sim")
            {
                ordem.garantia = "1";
            }
            else
            {
                ordem.garantia = "2";
            }

            if (txtLocal.Text == "Empresa")
            {
                ordem.local = "1";
            }
            if (txtLocal.Text == "Interno")
            {
                ordem.local = "2";
            }
            if (txtLocal.Text == "Residêncial")
            {
                ordem.local = "3";
            }
            new BusinessOrdem().salvar(ordem);
            MessageBox.Show("Ordem salva com Sucesso !!");
        }
        public void carregaDados()
        {
            txtIdOS.Text = BusinessAlteraOrdem.idOrdem.ToString();
            txtTecResponsavel.Text = BusinessAlteraOrdem.idTecnico.ToString();
            txtIdCliente.Text = BusinessAlteraOrdem.idCliente.ToString();
            txtIdCliente.Select();
            SendKeys.Send("{ENTER}");
            txtInfAdicionais.Text = BusinessAlteraOrdem.informacoesAdc.ToString();
            txtProblema.Text = BusinessAlteraOrdem.problemaConstatado.ToString();
            txtSolucao.Text = BusinessAlteraOrdem.solucaoServico.ToString();
            txtDefeito.Text = BusinessAlteraOrdem.defeitoReclamado.ToString();
            txtDataEmissao.Text = BusinessAlteraOrdem.dtCadastro.ToString();
            txtDataExecucao.Text = BusinessAlteraOrdem.prazoEntrega.ToString();
            txtDataExecucao.Text = BusinessAlteraOrdem.prazoEntrega.ToString();
            txtGarantia.Text = BusinessAlteraOrdem.garantia.ToString();
            txtLocal.Text = BusinessAlteraOrdem.local.ToString();
            txtDefeito.Text = BusinessAlteraOrdem.defeitoReclamado.ToString();
            txtEquipamento.Text = BusinessAlteraOrdem.equipamentoDeixado.ToString();
            txtInicio.Text = BusinessAlteraOrdem.inicio.ToString();
            txtFim.Text = BusinessAlteraOrdem.fim.ToString();
            txtFinalizacaoOrdem.Text = BusinessAlteraOrdem.finalizacao.ToString();
        }
        private void frmCadOrdemServico_Load(object sender, EventArgs e)
        {
            CarregarComboBox();
            txtFinalizacaoOrdem.Text = "";
            txtTecResponsavel.Select();
            txtDataEmissao.Text = DateTime.Now.ToString("dd/MM/yyyy");

            if(BusinessAlteraOrdem.idOrdem != null)
            {
                carregaDados();
                BusinessAlteraOrdem.idOrdem = null;
            }
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtTelefone.Text == "")
            {
                if (txtTelefone.Text.Length > 10)
                {
                    MessageBox.Show("Telefone Inválido, Por favor preencher corretamente !!");
                    txtTelefone.Text = "";
                    txtTelefone.Select();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtTelefone.Text != "")
                    {
                        txtTelefone.Mask = "(00)-0000-0000";
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                    else
                    {
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                }
            }
        }

        private void txtCelular_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtCelular.Text == "")
            {
                if (txtCelular.Text.Length > 11)
                {
                    MessageBox.Show("Celular Inválido, Por favor preencher corretamente !!");
                    txtCelular.Text = "";
                    txtCelular.Select();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtCelular.Text != "")
                    {
                        txtCelular.Mask = "(00)-00000-0000";
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                    else
                    {
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                }
            }
        }

        private void txtInicio_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtInicio.Text == "")
            {
                if (txtInicio.Text.Length > 4)
                {
                    MessageBox.Show("Hora Inválida, Por favor preencher corretamente !!");
                    txtInicio.Text = "";
                    txtInicio.Select();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtInicio.Text != "")
                    {
                        txtInicio.Mask = "00:00";
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                    else
                    {
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                }
            }
        }

        private void txtFim_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtFim.Text == "")
            {
                if (txtFim.Text.Length > 4)
                {
                    MessageBox.Show("Hora Inválida, Por favor preencher corretamente !!");
                    txtFim.Text = "";
                    txtFim.Select();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtFim.Text != "")
                    {
                        txtFim.Mask = "00:00";
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }

                    else
                    {
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                }
            }
        }

        private void txtFinalizacaoOrdem_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtFinalizacaoOrdem.Text == "")
            {
                if (txtFinalizacaoOrdem.Text.Length > 8)
                {
                    MessageBox.Show("Data Inválida, Por favor preencher corretamente !!");
                    txtFinalizacaoOrdem.Text = "";
                    txtFinalizacaoOrdem.Select();
                }
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txtFinalizacaoOrdem.Text != "")
                    {
                        txtFinalizacaoOrdem.Mask = "00/00/0000";
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                    else
                    {
                        SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                }
            }
        }

        private void txtDefeito_TextChanged(object sender, EventArgs e)
        {
            int totalValue = 0 + txtDefeito.TextLength;
            label24.Text = totalValue.ToString() + " de 1000";
        }

        private void txtEquipamento_TextChanged(object sender, EventArgs e)
        {
            int totalValue = 0 + txtEquipamento.TextLength;
            label27.Text = totalValue.ToString() + " de 1000";
        }

        private void txtSolucao_TextChanged(object sender, EventArgs e)
        {
            int totalValue = 0 + txtSolucao.TextLength;
            label33.Text = totalValue.ToString() + " de 1000";
        }

        private void txtProblema_TextChanged(object sender, EventArgs e)
        {
            int totalValue = 0 + txtProblema.TextLength;
            label36.Text = totalValue.ToString() + " de 1000";
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int totalValue = 0 + txtInfAdicionais.TextLength;
            label19.Text = totalValue.ToString() + " de 1000";
        }

        private void txtInfAdicionais_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtProblema_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtSolucao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtDefeito_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtEquipamento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtProdutos_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtProdutos_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtIdCliente_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtIdCliente_KeyDown(object sender, KeyEventArgs e)
        {
            BusinessOrdem ordem = new BusinessOrdem();
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(txtIdCliente.Text))
                {
                    var frmPesqCliente = new frmPesqCliente(this);
                    frmPesqCliente.ShowDialog();
                }
                else
                {
                    cliente = ordem.BuscaClienteID(Convert.ToInt32(txtIdCliente.Text.ToString()));
                    if(cliente == null)
                    {
                        MessageBox.Show("Cliente não encontrado !");
                        txtIdCliente.Text = "";
                    }
                    else
                    {
                    CarregaCliente(cliente);
                    SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
                    }
                }
            }
        }

        private void txtTecResponsavel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtContato_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtDataExecucao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtGarantia_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void txtLocal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send((e.Shift ? "+" : "") + "{TAB}");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtTecResponsavel.Text) || string.IsNullOrEmpty(txtIdCliente.Text) || string.IsNullOrEmpty(txtDefeito.Text))
            {
                MessageBox.Show("Campos em branco, por favor preenche-los !!");
                txtTecResponsavel.Select();
            }
            else
            {
            salvar();
            Close();
            }
        }

        private void txtTecResponsavel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            stiReport2.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BusinessAlteraOrdem.idOrdem = txtIdOS.Text;
            //Ordem. = txtNome.Text;
            new BusinessOrdem().Excluir(ordem);
            MessageBox.Show("A Ordem foi excluida com sucesso!");
            //limpar();
            Close();
        }
    }
}