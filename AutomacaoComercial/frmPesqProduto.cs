﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutomacaoComercial
{
    public partial class frmPesqProduto : Form
    {
        public frmPesqProduto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var frmCadProduto = new frmCadProduto();
            frmCadProduto.ShowDialog();
        }

        private void frmPesqProduto_Load(object sender, EventArgs e)
        {
            txtPesquisa.Select();
        }
    }
}
