﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.Win32;
using System.Data.SqlClient;
using Business;
using Dao;
using DFW;

namespace AutomacaoComercial
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            RegistryKey registro = Registry.CurrentUser.OpenSubKey("SISTEMA", true);
            if (registro == null)
            {
                //Se não existe Crio!
                registro = Registry.CurrentUser.CreateSubKey("SISTEMA");
                registro.SetValue("CAMINHO_PASTA", "D:\\SISTEMA\\");
                registro.SetValue("ESTACAO", "CAIXA_01");
                registro.SetValue("CAMINHO_SERVIDOR", "(LOCAL)");
                registro.SetValue("BANCO", "LOCAL");
            }
            
            //O sistema é executado uma vez só
            Process aProcess = Process.GetCurrentProcess();
            string aProcName = aProcess.ProcessName;
            if (Process.GetProcessesByName(aProcName).Length > 1)
            {
                MessageBox.Show("O programa já está em execução, Aguarde !", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //Mudar data para dd/MM/yyyy
            if (System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern != "dd/MM/yyyy")
            {
                MessageBox.Show("Mude no Painel de Controle para o Formato da data para dd/MM/yyyy.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Process.GetCurrentProcess().Kill();
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new frmApresentacao());
            Application.Run(new frmPrincipal());
        }
    }
}
