﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class ComboBoxTecnico
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public ComboBoxTecnico() { }
        public ComboBoxTecnico(string text, object value)
        {
            this.Text = text;
            this.Value = value;
        }
        public override string ToString()
        {
            return Text;
        }
    }
}