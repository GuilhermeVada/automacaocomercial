﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Agenda
    {
        public int idCliente { get; set; }
        public string nome { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string anotacoes { get; set; }

    }
}
