﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Cliente
    {
        public int idCliente { get; set; }
        public string nome { get; set; }
        public string nascimento { get; set; }
        public string rg { get; set; }
        public string cpf { get; set; }
        public string naturalidade { get; set; }
        public string sexo { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string fax { get; set; }
        public string estadoCivil { get; set; }
        public string contato { get; set; }
        public string endereco { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }
        public string pontoReferencia { get; set; }
        public string complemento { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string ativo { get; set; }
        public string inscEstadual { get; set; }
        public string cnpj { get; set; }
        public string nacionalidade { get; set; }

    }
}
