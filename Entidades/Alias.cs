﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Alias
    {
        public int idAlias { get; set; }
        public string empresa { get; set; }
        public string banco { get; set; }
        public string servidor { get; set; }        
    }
}
