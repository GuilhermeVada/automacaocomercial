﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Ordem
    {
        public int idOrdem { get; set; }
        public int idTecnico { get; set; }
        public int idCliente { get; set; }
        public string dtCadastro { get; set; }
        public string prazoEntrega { get; set; }
        public string situacao { get; set; }
        public string tipoAtendimento { get; set; }
        public string garantia { get; set; }
        public string garantiaAte { get; set; }
        public string local { get; set; }
        public string defeitoReclamado { get; set; }
        public string equipamentoDeixado { get; set; }
        public string informacoesAdc { get; set; }
        public string problemaConstatado { get; set; }
        public string solucaoServico { get; set; }
        public string valorProduto { get; set; }
        public string valorServico { get; set; }
        public string valorDeslocamento { get; set; }
        public string totalOrdem { get; set; }
        public string finalizacao { get; set; }
        public string inicio { get; set; }
        public string fim { get; set; }
        public string contato { get; set; }

    }
}
