﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Fornecedor
    {
        public int idFornecedor { get; set; }
        public string razaoSocial { get; set; }
        public string fantasia { get; set; }
        public string inscEstadual { get; set; }
        public string cnpj { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }
        public string ramal { get; set; }
        public string fax { get; set; }
        public string contato { get; set; }
        public string endereco { get; set; }
        public string numero { get; set; }
        public string bairro { get; set; }
        public string pontoReferencia { get; set; }
        public string complemento { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string cep { get; set; }
        public string status { get; set; }
    }
}
